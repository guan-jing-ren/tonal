#include "class.hpp"
#include "closest_match.hpp"
#include "entity_match.hpp"
#include "evaluation.hpp"
#include "function.hpp"
#include "list.hpp"
#include "module.hpp"
#include "name.hpp"
#include "program.hpp"
#include "resolve_routine.hpp"
#include "variable.hpp"

#include <bitfield.hpp>
#include <functor.hpp>
#include <variant.hpp>

#include <chrono>
#include <deque>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <thread>
#include <unordered_map>
#include <unordered_set>

using namespace tonal;
using namespace gut;
using namespace std;

struct EditorPosition {};
struct FilePosition {};
struct ExpressionPosition {};

namespace tonal {
deque<byte> to_bytes(const string_view &s);

template <typename... L> unique_ptr<List> Program::operator()(L &&... l) {
  auto maybe_token = [this](auto l) {
    constexpr type ltype = type_v<decltype(l)>.remove_cvref();
    if constexpr (ltype == type_v<const char *> || ltype == type_v<string_view>)
      return token_pool[l];
    else
      return move(l);
  };
  auto list = make_unique<List>();
  (..., list->cells.push_back({maybe_token(forward<L>(l))}));
  return list;
}

shared_ptr<Module> boot; // Bootstrap module for language features
shared_ptr<Module> global;
shared_ptr<Module> compiler;

// Bootstrap classes
shared_ptr<Class> list;
shared_ptr<Class> archetype; // module, class, function, variable
shared_ptr<Class> concept;   // tuple, union, enum, array
shared_ptr<Class> string;
shared_ptr<Class> integer;
shared_ptr<Class> real;

// Bootstrap functions
shared_ptr<Function> equals;

// Bootstrap list, string, integer and real functions
shared_ptr<Function> less_than;

// Bootstrap list and string functions
shared_ptr<Variable> count;
shared_ptr<Function> contains;

// Name management functions
shared_ptr<Function> module;
shared_ptr<Function> use; // First instance in class and function means capture
                          // list ; then if is a name, imports the symbol; if it
                          // is a string, points to a unique file as code - code
                          // must be a well-formed entity body (module, class,
                          // function body) and only referred to once. Not sure
                          // what to do about source on networks.
shared_ptr<Function> undefine; // Undefines a symbol
shared_ptr<Function>
    embed; // Reads given file contents as non-code string value

// Special free variables
shared_ptr<Variable> placeholder; // @

// Special type variables
shared_ptr<Variable> name;
shared_ptr<Variable> parameters;
shared_ptr<Variable> encloser;
shared_ptr<Variable> overloads;
shared_ptr<Variable> readers;
shared_ptr<Variable> writers;
shared_ptr<Variable> evaltype;
shared_ptr<Variable> members;
shared_ptr<Variable> bases;
shared_ptr<Variable> derivations;

// Special class functions
shared_ptr<Function> default_constructor;
shared_ptr<Function> copy_constructor;
// shared_ptr<Function> move_constructor;
shared_ptr<Function> destructor;
shared_ptr<Function> copy_assignment;
// shared_ptr<Function> move_assignment;
shared_ptr<Function> memberwise_constructor;
shared_ptr<Function> memberwise_assignment;
shared_ptr<Function> transpose_constructor;
shared_ptr<Function> transpose_assignment;
shared_ptr<Function> transpose_destructor;

// Special scoped variables
shared_ptr<Variable> self_type;
shared_ptr<Variable> self_object;
shared_ptr<Variable> self_size;
shared_ptr<Variable> self_alignment;
shared_ptr<Variable> self_free_bits;
shared_ptr<Variable> compile_date;
shared_ptr<Variable> compile_time;
shared_ptr<Variable> compile_file;
shared_ptr<Variable> compile_line;
shared_ptr<Variable> compile_position;
shared_ptr<Variable> compile_nesting;
shared_ptr<Variable> compile_counter;

// Special unscoped variable
shared_ptr<Variable> compile_random;
shared_ptr<Variable> compiler_system;
shared_ptr<Variable> compiler_version;
shared_ptr<Variable> compiler_language;
shared_ptr<Variable> compiler_endianness;
shared_ptr<Variable> compiler_pointer_size;
shared_ptr<Variable> compiler_byte_size;
} // namespace tonal

template <typename Ent> void print(const Program &p, Ent &&e, count_t ind = 0) {
  constexpr type entity_type = type_v<decltype(e)>.remove_cvref();
  std::string indent(ind, '\t');
  cout << indent <<
      [entity_type]() {
        if constexpr (entity_type == type_v<shared_ptr<Module>>)
          return "Module";
        else if constexpr (entity_type == type_v<shared_ptr<Class>>)
          return "Class";
        else if constexpr (entity_type == type_v<shared_ptr<Function>>)
          return "Function";
        else if constexpr (entity_type == type_v<shared_ptr<Variable>>)
          return "Variable";
      }()
       << ": " << *e->name.lock() << '\n';

  for (auto &&param : e->parameters) {
    cout << indent << "\t\tParameter: " << *param.token << '\n';
    if (param.evaluation.type)
      cout << indent << "\t\t\tEvaluation: "
           << *param.evaluation.type.value()
                   .reduce([](auto &&eval) { return eval.lock()->name.lock(); })
                   .value()
           << '\n';
  }

  if constexpr (entity_type == type_v<shared_ptr<Module>> ||
                entity_type == type_v<shared_ptr<Class>>) {
    for (auto &&memvar : e->member_variables)
      print(p, memvar, ind + 1);
    cout << '\n';
    for (auto &&memfn : e->member_functions)
      print(p, memfn, ind + 1);
    cout << '\n';
    for (auto &&memcls : e->member_classes)
      print(p, memcls, ind + 1);
    cout << '\n';
    if constexpr (entity_type == type_v<shared_ptr<Module>>)
      for (auto &&memmod : e->member_modules)
        print(p, memmod, ind + 1);
  } else if constexpr (entity_type == type_v<shared_ptr<Function>> ||
                       entity_type == type_v<shared_ptr<Variable>>) {
    if (e->evaluation.type)
      cout << indent << "\t\tEvaluation: "
           << *e->evaluation.type.value()
                   .reduce([](auto &&eval) { return eval.lock()->name.lock(); })
                   .value()
           << '\n';
    for (auto &[_, archetype] : p.overloads)
      archetype.each([&e, &indent](auto &&entity) {
        if (entity->encloser)
          entity->encloser.value().each(
              [&e, &indent, &entity](auto &&encloser) {
                constexpr type e_type = type_v<decltype(e)>.remove_cvref();
                constexpr type encloser_type =
                    type_v<decltype(encloser.lock())>.remove_cvref();
                if constexpr (e_type == encloser_type)
                  if (encloser.lock() == e)
                    cout << indent << '\t' << *entity->name.lock() << '\n';
              });
      });
  }
}

unique_ptr<List> param_names_test(Program &program, size_t level) {
  auto params = program();
  generate_n(back_inserter(params->cells), level, [&program, i = 0]() mutable {
    return ListCell{program(string_view{"p-" + to_string(++i)})};
  });
  return params;
}

unique_ptr<List> var_names_test(Program &program, size_t level,
                                deque<int> &counts) {
  if (counts.size() == level)
    counts.push_back(0);
  return program(
      "let",
      string_view{"v-" + to_string(level) + '-' + to_string(++counts[level])},
      param_names_test(program, level));
}
unique_ptr<List> func_names_test(Program &program, size_t level,
                                 deque<int> &counts) {
  if (counts.size() == level)
    counts.push_back(0);
  return program(
      "function",
      string_view{"f-" + to_string(level) + '-' + to_string(++counts[level])},
      param_names_test(program, level), program());
}
unique_ptr<List> class_names_test(Program &program, size_t level,
                                  deque<int> &counts) {
  if (level == 5)
    return program(
        "class",
        string_view{"c-" + to_string(level) + '-' + to_string(++counts[level])},
        param_names_test(program, level), program(), program());
  if (counts.size() == level)
    counts.push_back(0);
  return program(
      "class",
      string_view{"c-" + to_string(level) + '-' + to_string(++counts[level])},
      param_names_test(program, level), program(), program(),
      var_names_test(program, level + 1, counts),
      func_names_test(program, level + 1, counts),
      class_names_test(program, level + 1, counts),
      class_names_test(program, level + 1, counts),
      func_names_test(program, level + 1, counts),
      var_names_test(program, level + 1, counts));
}
unique_ptr<List> mod_names_test(Program &program, size_t level,
                                deque<int> &counts) {
  if (level == 5)
    return program(
        "module",
        string_view{"c-" + to_string(level) + '-' + to_string(++counts[level])},
        param_names_test(program, level));
  if (counts.size() == level)
    counts.push_back(0);
  return program(
      "module",
      string_view{"m-" + to_string(level) + '-' + to_string(++counts[level])},
      param_names_test(program, level),
      var_names_test(program, level + 1, counts),
      func_names_test(program, level + 1, counts),
      class_names_test(program, level + 1, counts),
      class_names_test(program, level + 1, counts),
      mod_names_test(program, level + 1, counts),
      mod_names_test(program, level + 1, counts),
      class_names_test(program, level + 1, counts),
      class_names_test(program, level + 1, counts),
      func_names_test(program, level + 1, counts),
      var_names_test(program, level + 1, counts));
}

unique_ptr<List> names_test(Program &program) {
  deque<int> counts;
  return mod_names_test(program, 0, counts);
}

extern std::string to_string(const unique_ptr<List> &);
void regression_tests() {
#define TEST_STRINGIZE2(value) #value
#define TEST_STRINGIZE(value) TEST_STRINGIZE2(value)
#define TEST(condition, message)                                               \
  if (!(condition))                                                            \
  throw runtime_error{TEST_STRINGIZE(__LINE__) ": " message}

  Program program;
  TEST(program.token_pool["Hello"] == program.token_pool["Hello"],
       "String pool from equal string should be equal.");
  TEST(*program.token_pool["Hello"] == *program.token_pool["Hello"],
       "String pool from equal string should have equal content.");
  TEST(program.token_pool["Hello"] != program.token_pool["Hello!"],
       "String pool from equal string should not be equal.");
  TEST(*program.token_pool["Hello"] != *program.token_pool["Hello!"],
       "String pool from equal string should have unequal content.");

  Evaluation param, arg;
  ClosestMatch closest_match;
  EntityMatch entity_match;

  TEST(closest_match.equal_value_and_type(),
       "Must start with equal value and type match.");
  auto cl = make_shared<Class>();
  param.type = cl;
  arg.type = cl;
  entity_match(param, arg, closest_match);
  TEST(!closest_match.equal_value(), "Expected unequal value match.");
  TEST(closest_match.equal_type(), "Expected equal type match.");
  closest_match = {};
  param.value = 42;
  arg.value = 42;
  entity_match(param, arg, closest_match);
  TEST(closest_match.equal_value_and_type(),
       "Expected equal value and equal type match.");
  closest_match = {};
  param.value = 42.0l;
  entity_match(param, arg, closest_match);
  TEST(closest_match.not_substitutable(),
       "Expected not substitutable due to unequal value.");
  closest_match = {};
  param.value.reset();
  entity_match(param, arg, closest_match);
  TEST(closest_match.equal_type(), "Expected equal type match.");

  auto test_list = program("+", "111", "+111", "-111", "0b111", "0o111",
                           "0d111", "0x111", "0z111",
                           program("-0b111", "-0o111", "-0d111", "-0x111",
                                   "-0z111", "11.1", "+11.1"),
                           "-11.1", "0d11.1", "0x11.1", "-0d11.1", "-0x11.1",
                           R"***tonal***('-0x11.1')***tonal***",
                           R"***tonal***(u8'-0x11.1')***tonal***",
                           R"***tonal***(u16'-0x11.1')***tonal***",
                           R"***tonal***(u32'-0x11.1')***tonal***",
                           program(R"***tonal***(R'(-0x11.1)')***tonal***",
                                   R"***tonal***(u8R'(-0x11.1)')***tonal***",
                                   R"***tonal***(u16R'(-0x11.1)')***tonal***",
                                   R"***tonal***(u32R'(-0x11.1)')***tonal***"),
                           R"***tonal***(R'!!!(-0x11.1)!!!')***tonal***",
                           R"***tonal***(u8R'!!!(-0x11.1)!!!')***tonal***",
                           R"***tonal***(u16R'!!!(-0x11.1)!!!')***tonal***",
                           R"***tonal***(u32R'!!!(-0x11.1)!!!')***tonal***");
  auto eval_list = program.eval(test_list);
  cout << to_string(eval_list) << '\n';

  auto names_list = names_test(program);
  cout << "Names list: " << to_string(names_list) << '\n';
  Program::ParseQueue pq;
  program.eval_member_module(names_list, pq, 0);
  pq.run();

  for (auto &[_, archetype] : program.overloads)
    archetype.each([&program](auto &&entity) {
      if (entity->name.lock()->parent.expired())
        print(program, entity);
    });

  enum class ResolveState { A, B, C, D, E, F, G };
  ResolveRoutine rr;
  rr.state(type_v<ntuple<constant<ResolveState::A>, int>>,
           [](auto &&, auto &&in) -> any {
             if (in.get(index_v<1>) % 2) {
               cout << "A->B" << '\n';
               return ntuple{constant_v<ResolveState::B>,
                             in.get(index_v<1>) + in.get(index_v<1>) + 1};
             }

             cout << "A->C" << '\n';
             return ntuple{constant_v<ResolveState::C>,
                           in.get(index_v<1>) + in.get(index_v<1>) + 2};
           });
  rr.state(type_v<ntuple<constant<ResolveState::B>, int>>,
           [](auto &&ctx, auto &&in) -> any {
             cout << "B->D" << '\n';
             return ctx.yield(
                 ntuple{constant_v<ResolveState::D>,
                        in.get(index_v<1>) * (in.get(index_v<1>) + 1)});
           });
  rr.state(type_v<ntuple<constant<ResolveState::C>, int>>, [](auto &&ctx,
                                                              auto &&in) {
    cout << "C->D" << '\n';
    return ctx.yield(ntuple{constant_v<ResolveState::D>,
                            in.get(index_v<1>) * (in.get(index_v<1>) + 2)});
  });
  rr.state(type_v<ntuple<constant<ResolveState::E>, int>>, [](auto &&ctx,
                                                              auto &&in) {
    if (in.get(index_v<1>) % 2) {
      cout << "E->A|F" << '\n';
      return ctx.fork(
          ResolveRoutine::ProgressPolicy::EAGER_TO_YIELD,
          ntuple{constant_v<ResolveState::A>, in.get(index_v<1>)},
          ntuple{constant_v<ResolveState::F>, in.get(index_v<1>) + 1});
    }

    cout << "E->F+" << '\n';
    buffer out{{ntuple{constant_v<ResolveState::F>, in.get(index_v<1>)},
                ntuple{constant_v<ResolveState::F>, in.get(index_v<1>) + 1}}};
    return ctx.map(ResolveRoutine::ProgressPolicy::ROUND_ROBIN, out.begin(),
                   out.end());
  });
  rr.state(type_v<ntuple<constant<ResolveState::F>, int>>, [](auto &&ctx,
                                                              auto &&in) {
    cout << "F." << '\n';
    return ctx.yield(ntuple{constant_v<ResolveState::G>, in.get(index_v<1>)});
  });
  rr.join(make_type_list<ntuple<constant<ResolveState::D>, int>,
                         ntuple<constant<ResolveState::G>, int>>(),
          [](auto &&, auto &&a, auto &&f) {
            return a.get(index_v<1>) * f.get(index_v<1>);
          });
  rr.reduce(type_v<ntuple<constant<ResolveState::G>, int>>,
            [](auto &&, auto &&a, auto &&f) {
              return accumulate(a, f, 1., [](auto &&result, auto &&item) {
                return result * item.get(index_v<1>);
              });
            });
  cout << any_cast<ntuple<constant<ResolveState::D>, int> &&>(
              rr.run(ntuple{constant_v<ResolveState::A>, 1}))
              .get(index_v<1>)
       << '\n';
  cout << any_cast<ntuple<constant<ResolveState::D>, int> &&>(
              rr.run(ntuple{constant_v<ResolveState::A>, 2}))
              .get(index_v<1>)
       << '\n';
  cout << any_cast<int>(rr.run(ntuple{constant_v<ResolveState::E>, 1})) << '\n';
  cout << any_cast<double>(rr.run(ntuple{constant_v<ResolveState::E>, 2}))
       << '\n';

  enum class CallWithCC { A, B, C };
  rr.state(type_v<ntuple<constant<CallWithCC::A>, int>>,
           [](auto &&ctx, auto &&i) {
             auto cc = [](auto &&cc) {
               cout << "?->A\n";
               return ntuple{constant_v<CallWithCC::A>,
                             static_cast<int>(any_cast<double>(cc))};
             };
             if (auto result = i.get(index_v<1>); result % 2) {
               cout << "A->B with: " << i.get(index_v<1>) << '\n';
               return ctx.call_cc(constant_v<CallWithCC::B>,
                                  static_cast<double>(i.get(index_v<1>)), cc);
             } else if (result < 6000) {
               cout << "A->C with: " << i.get(index_v<1>) << '\n';
               return ctx.call_cc(constant_v<CallWithCC::C>,
                                  static_cast<double>(i.get(index_v<1>)), cc);
             } else
               return any{result};
           });
  rr.with_cc(constant_v<CallWithCC::B>, type_v<double>, [](auto &&i) {
    cout << "B->?\n";
    return i + 3.;
  });
  rr.with_cc(constant_v<CallWithCC::C>, type_v<double>, [](auto &&i) {
    cout << "C->?\n";
    return i + 5.;
  });
  rr.run(ntuple{constant_v<CallWithCC::A>, 0});

  cout << "Make name test: " << *program.make_name({"a", "b", "c", "d"})
       << '\n';

  {
    Program program;
    program.start_class();
    program.give_entity_name("cl1-1");
    program.start_class();
    program.give_entity_name("cl1");
    program.start_function();
    program.give_entity_name("a");
    program.pend_entity();
    program.start_function();
    program.give_entity_name("a");
    program.pend_entity();
    program.start_function();
    program.give_entity_name("a");
    program.pend_entity();
    program.start_function();
    program.give_entity_name("b");
    program.pend_entity();
    program.start_function();
    program.give_entity_name("b");
    program.pend_entity();
    program.start_function();
    program.give_entity_name("b");
    program.pend_entity();
    program.start_function();
    program.give_entity_name("b");
    program.pend_entity();
    program.start_function();
    program.give_entity_name("b");
    program.pend_entity();
    program.pend_entity();
    program.pend_entity();

    program.start_class();
    program.give_entity_name("cl2-1");
    program.start_class();
    program.give_entity_name("cl1");
    program.start_function();
    program.give_entity_name("a");
    program.pend_entity();
    program.start_function();
    program.give_entity_name("a");
    program.pend_entity();
    program.start_function();
    program.give_entity_name("a");
    program.pend_entity();
    program.start_function();
    program.give_entity_name("a");
    program.pend_entity();
    program.start_function();
    program.give_entity_name("a");
    program.pend_entity();
    program.start_function();
    program.give_entity_name("b");
    program.pend_entity();
    program.start_function();
    program.give_entity_name("b");
    program.pend_entity();
    program.start_function();
    program.give_entity_name("b");
    program.pend_entity();
    program.start_function();
    program.give_entity_name("b");
    program.pend_entity();
    program.start_function();
    program.give_entity_name("b");
    program.pend_entity();
    program.pend_entity();
    program.pend_entity();

    auto ends_with_test =
        [&program](const string_view &test_name,
                   const shared_ptr<const Name> &name,
                   const unordered_map<shared_ptr<const Name>, count_t>
                       &expected_overload_count) {
          cout << test_name << " test:\n";
          auto names = program.ends_with(name);
          TEST(!names.empty() || expected_overload_count.empty(),
               "Expected some match, but got none.");
          TEST(is_permutation(
                   expected_overload_count.cbegin(),
                   expected_overload_count.cend(), names.cbegin(), names.cend(),
                   [](auto &&name, auto &&name_count) {
                     if constexpr (type_v<decltype(name)>.remove_cvref() ==
                                   type_v<shared_ptr<const Name>>)
                       return name == name_count.first;
                     else if constexpr (
                         type_v<decltype(name_count)>.remove_cvref() ==
                         type_v<shared_ptr<const Name>>)
                       return name.first == name_count;
                     else
                       return name.first == name_count.first;
                   }),
               "Missing some fully qualified names.");
          for (auto &&name : names)
            TEST(expected_overload_count.at(name) ==
                     [](auto &&range) {
                       return distance(range.first, range.second);
                     }(program.overloads.equal_range(name)),
                 "Expected " + to_string(expected_overload_count.at(name)) +
                     " overloads");
        };

    ends_with_test("Ends with cl1.a", program.make_name({"cl1", "a"}),
                   {{{program.make_name({"cl1-1", "cl1", "a"}), 3},
                     {program.make_name({"cl2-1", "cl1", "a"}), 5}}});

    ends_with_test("Ends with cl2-1.cl1.b",
                   program.make_name({"cl2-1", "cl1", "b"}),
                   {{{program.make_name({"cl2-1", "cl1", "b"}), 5}}});

    ends_with_test("Ends with cl2-1.cl1", program.make_name({"cl2-1", "cl1"}),
                   {{{program.make_name({"cl2-1", "cl1"}), 1}}});

    ends_with_test("Ends with cl1", program.make_name({"cl1"}),
                   {{{program.make_name({"cl1-1", "cl1"}), 1},
                     {program.make_name({"cl2-1", "cl1"}), 1}}});

    ends_with_test("Ends with m.cl2-1.cl1",
                   program.make_name({"m", "cl2-1", "cl1"}), {});
  }

#undef TEST
#undef TEST_STRINGIZE
#undef TEST_STRINGIZE2
}

int main() {
  thread test_runner{regression_tests};

  struct T {
    ~T() { cout << "Destructing T\n"; }
  };

  [[maybe_unused]] variant<shared_ptr<T>, shared_ptr<T>> v{index_v<1>,
                                                           make_shared<T>()};
  cout << "v index: " << static_cast<count_t>(v.index()) << '\n';
  cout << "Share count: " << v.value(index_v<0>).use_count() << '\n';
  {
    decltype(v) v2 = {index_v<0>, v.value(index_v<0>)};
    cout << "v2 index: " << static_cast<count_t>(v2.index()) << '\n';
    cout << "Share count: " << v2.value(index_v<0>).use_count() << '\n';
    {
      gut::optional o = v.value(index_v<0>);
      cout << "o index: " << static_cast<count_t>(o) << '\n';
      cout << "Share count: " << o.value().use_count() << '\n';
    }
  }
  cout << "Share count: " << v.value(index_v<0>).use_count() << '\n';

  test_runner.join();

  return 0;
}
