#include "closest_match.hpp"

#include <ostream>

using namespace gut;
using namespace std;

namespace tonal {
bool ClosestMatch::equal_value() const {
  return match_flags[index_v<0>] == 0b1;
}

bool ClosestMatch::equal_type() const {
  return match_flags[index_v<1>] == 0b10;
}

bool ClosestMatch::equal_value_and_type() const {
  return equal_value() && equal_type();
}

bool ClosestMatch::compatible_type() const {
  return match_flags[index_v<1>] == 0b1;
}

bool ClosestMatch::equal_value_and_compatible_type() const {
  return equal_value() && compatible_type();
}

bool ClosestMatch::incompatible() const {
  return !equal_value() && !equal_type() && !compatible_type();
}

bool ClosestMatch::param_substitutable() const {
  return match_flags[index_v<2>] == 0b10;
}

bool ClosestMatch::param_unspecified() const {
  return match_flags[index_v<2>] == 0b1;
}

bool ClosestMatch::equal_name() const {
  return match_flags[index_v<3>] == 0b10;
}

bool ClosestMatch::name_unspecified() const {
  return match_flags[index_v<3>] == 0b1;
}

bool ClosestMatch::param_name_substitutable() const {
  return param_substitutable() && name_unspecified();
}

bool ClosestMatch::substitutable() const {
  return param_unspecified() && name_unspecified();
}

bool ClosestMatch::not_substitutable() const {
  return incompatible() && !param_substitutable() && !param_unspecified() &&
         !equal_name() && !name_unspecified();
}

void ClosestMatch::set_unequal_value() { match_flags[index_v<0>] = 0; }

void ClosestMatch::set_compatible_type() {
  if (auto &&field = match_flags[index_v<1>]; field > 1)
    field = 1;
}

void ClosestMatch::set_incompatible_type() {
  if (!incompatible()) {
    set_unequal_value();
    match_flags[index_v<1>] = 0;
    match_flags[index_v<2>] = 2;
    match_flags[index_v<3>] = 2;
  }
}

void ClosestMatch::set_param_unspecified() {
  set_incompatible_type();
  if (auto &&field = match_flags[index_v<2>]; field > 1)
    field = 1;
}

void ClosestMatch::set_name_unspecified() {
  set_incompatible_type();
  if (auto &&field = match_flags[index_v<3>]; field > 1)
    field = 1;
}

void ClosestMatch::set_unsubstitutable() { match_flags = {0, 0, 0, 0}; }

bool ClosestMatch::operator<(const ClosestMatch &that) const {
  count_t cmp = 0;
  for (count_t i = 6; cmp == 0 && i; --i)
    cmp = [](count_t self, count_t that) {
      return self - that;
    }(match_flags[i], that.match_flags[i]);
  return cmp < 0;
}

bool ClosestMatch::operator>(const ClosestMatch &that) const {
  return that < *this;
}

ostream &operator<<(ostream &out, const ClosestMatch &cm) {
  if (cm.equal_value_and_type())
    out << "Equal value and type.";
  else if (cm.equal_type())
    out << "Equal type.";
  if (cm.equal_value_and_compatible_type())
    out << "Equal value and compatible type.";
  else if (cm.compatible_type())
    out << "Compatible type.";
  if (cm.incompatible())
    out << "Incompatible.";
  if (cm.param_name_substitutable())
    out << "Param name substitutable.";
  else if (cm.equal_name())
    out << "Equal name.";
  else if (cm.param_substitutable())
    out << "Param  substitutable.";
  else if (cm.param_unspecified())
    out << "Param name unspecified.";
  else if (cm.name_unspecified())
    out << "Name unspecified.";
  if (cm.substitutable())
    out << "Substitutable.";
  if (cm.not_substitutable())
    out << "Not substitutable.";
  return out;
}
} // namespace tonal
