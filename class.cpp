#include "class.hpp"

using namespace std;

namespace tonal {
void Class::set_encloser(weak_ptr<Module> encloser) {
  this->encloser = encloser;
}

void Class::set_encloser(weak_ptr<Class> encloser) {
  this->encloser = encloser;
}

void Class::set_encloser(weak_ptr<Function> encloser) {
  this->encloser = encloser;
}

void Class::add_member(shared_ptr<Class> member) {
  member_classes.emplace_back(move(member));
}

void Class::add_member(shared_ptr<Function> member) {
  member_functions.emplace_back(move(member));
}

void Class::add_member(shared_ptr<Variable> member) {
  member_variables.emplace_back(move(member));
}
} // namespace tonal
