#include "variable.hpp"

using namespace std;

namespace tonal {
bool Variable::is_fully_defined() const {
  return Entity::is_fully_defined() && evaluation.is_fully_defined();
}

void Variable::set_encloser(weak_ptr<Module> encloser) {
  this->encloser = encloser;
}

void Variable::set_encloser(weak_ptr<Class> encloser) {
  this->encloser = encloser;
}

void Variable::set_encloser(weak_ptr<Function> encloser) {
  this->encloser = encloser;
}
} // namespace tonal