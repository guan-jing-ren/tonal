#pragma once

#include "closest_match.hpp"

#include <variant.hpp>

#include <deque>
#include <memory>

namespace tonal {
struct EntityMatch {
  template <typename LeftValue, typename ArgValue>
  void operator()(const LeftValue &, const ArgValue &,
                  ClosestMatch &max_match) const {
    max_match.set_unequal_value();
  }

  template <typename Value>
  void operator()(const Value &param, const Value &arg,
                  ClosestMatch &max_match) const {
    if (arg != param)
      max_match.set_unequal_value();
  }

  void operator()(const gut::something &, const gut::something &,
                  ClosestMatch &max_match) const;

  void operator()(const struct Evaluation &param, const struct Evaluation &arg,
                  ClosestMatch &max_match) const;

  void operator()(const struct ListCell &param, const struct ListCell &arg,
                  ClosestMatch &max_match) const;

  void operator()(const std::unique_ptr<struct List> &param,
                  const std::unique_ptr<struct List> &arg,
                  ClosestMatch &max_match) const;

  template <typename... Members>
  void operator()(const gut::variant<Members...> &param,
                  const gut::variant<Members...> &arg,
                  ClosestMatch &max_match) const {
    param.map([this, &arg, &max_match](const auto &param) {
      arg.map([this, &param, &max_match](const auto &arg) {
        (*this)(param, arg, max_match);
      });
    });
  }

  template <typename Pointee>
  void operator()(const std::shared_ptr<Pointee> &param,
                  const std::shared_ptr<Pointee> &arg,
                  ClosestMatch &max_match) const {
    if constexpr (gut::type_v<Pointee>.remove_cvref() ==
                  gut::type_v<struct Class>) {
      if (arg != param)
        (*this)(static_cast<const struct Class &>(*param),
                static_cast<const struct Class &>(*arg), max_match);
    } else if constexpr (
        gut::type_v<Pointee>.remove_cvref().template is_derived_from<struct Entity>()) {
      if (arg != param)
        (*this)(static_cast<const struct Entity &>(*param),
                static_cast<const struct Entity &>(*arg), max_match);
    } else if (arg != param)
      max_match.set_unequal_value();
  }

  template <typename Pointee>
  void operator()(const std::weak_ptr<Pointee> &param,
                  const std::weak_ptr<Pointee> &arg,
                  ClosestMatch &max_match) const {
    (*this)(param.lock(), arg.lock(), max_match);
  }

  template <typename Element>
  void operator()(const std::deque<Element> &param,
                  const std::deque<Element> &arg,
                  ClosestMatch &max_match) const {
    for (auto param_first = param.cbegin(), param_last = param.cend(),
              arg_first = arg.cbegin(), arg_last = arg.cend();
         param_first != param_last && arg_first != arg_last;
         ++param_first, ++arg_first) {
      (*this)(*param_first, *arg_first, max_match);
      if (max_match.not_substitutable())
        return;
    }
  }

  void operator()(const struct Parameter &param, const struct Parameter &arg,
                  ClosestMatch &max_match) const;

  void operator()(const struct Class &param, const struct Class &arg,
                  ClosestMatch &max_match) const;

  void operator()(const struct Entity &param, const struct Entity &arg,
                  ClosestMatch &max_match) const;
};
} // namespace tonal
