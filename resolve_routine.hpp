#pragma once

#include "functor.hpp"

#include <any>
#include <deque>
#include <functional>
#include <queue>
#include <typeindex>
#include <unordered_map>

namespace tonal {
struct RoutineContext {
  enum class ProgressPolicy { EAGER_TO_YIELD, ROUND_ROBIN };

  struct Split {
    ProgressPolicy policy;
    std::deque<std::any> in;
  };

  struct Resume {
    std::function<std::any(std::any)> continuation;
  };

  struct Yield {
    std::any value; // Check same type as in state;
  };

  template <typename... CoInType>
  std::any fork(ProgressPolicy policy, CoInType... co_in) const {
    return Split{policy, {{{std::move(co_in)}...}}};
  }

  template <typename CoInBegin, typename CoInEnd>
  std::any map(ProgressPolicy policy, CoInBegin begin, CoInEnd end) const {
    Split split{policy, {}};
    split.in.insert(split.in.end(), begin, end);
    return split;
  }

  template <typename CoOutType> std::any yield(CoOutType out) const {
    return Yield{{std::move(out)}};
  }

  template <typename CoIdType, typename CoInType, typename CoFuncType>
  std::any call_cc(CoIdType, CoInType in, CoFuncType fn) const {
    return gut::ntuple{CoIdType{}, std::move(in), Resume{{std::move(fn)}}};
  };
};

struct ResolveRoutine : RoutineContext {
  std::unordered_map<std::type_index, std::function<std::any(
                                          const RoutineContext &, std::any &)>>
      states;
  struct JoinHash {
    std::uint32_t operator()(const std::deque<std::type_index> &joins) const;
  };
  std::unordered_map<
      std::deque<std::type_index>,
      std::function<std::any(const RoutineContext &, std::deque<std::any>)>,
      JoinHash>
      joins;
  std::unordered_map<
      std::type_index,
      std::function<std::any(const RoutineContext &, std::deque<std::any>)>>
      reduces;

  template <typename InType, typename FuncType>
  void state(gut::type<InType>, FuncType fn) {
    static_assert(sizeof(FuncType) == 1 && std::is_empty_v<FuncType>);
    states[typeid(InType)] = [fn = std::move(fn)](const RoutineContext &ctx,
                                                  std::any &in) -> std::any {
      return fn(ctx, std::move(std::any_cast<InType &>(in)));
    };
  }

  template <typename... InTypes, typename FuncType>
  void join(gut::type_list<InTypes...>, FuncType fn) {
    static_assert(sizeof(FuncType) == 1 && std::is_empty_v<FuncType>);
    joins[std::deque<std::type_index>{
        {typeid(typename decltype(get_type(InTypes{}))::raw_type)...}}] =
        [fn = std::move(fn)](const RoutineContext &ctx,
                             std::deque<std::any> in) -> std::any {
      return gut::type_list<InTypes...>().reduce([&fn, &ctx,
                                                  &in](auto... i) mutable {
        return fn(
            ctx,
            std::move(std::any_cast<typename decltype(get_type(i))::raw_type &>(
                in[get_index(i)]))...);
      });
    };
  }

  template <typename InType, typename FuncType>
  void reduce(gut::type<InType>, FuncType fn) {
    static_assert(sizeof(FuncType) == 1 && std::is_empty_v<FuncType>);
    reduces[typeid(InType)] =
        [fn = std::move(fn)](const RoutineContext &ctx,
                             std::deque<std::any> in) -> std::any {
      std::deque<InType> intermediary;
      std::transform(
          std::move_iterator{in.begin()}, std::move_iterator{in.end()},
          back_inserter(intermediary),
          [](auto &&in) { return std::move(std::any_cast<InType &>(in)); });
      return fn(ctx, std::move_iterator{intermediary.begin()},
                std::move_iterator{intermediary.end()});
    };
  }

  template <typename CoIdType, typename InType, typename FuncType>
  void with_cc(CoIdType, gut::type<InType>, FuncType fn) {
    static_assert(sizeof(FuncType) == 1 && std::is_empty_v<FuncType>);
    states[typeid(gut::ntuple<CoIdType, InType, Resume>)] =
        [fn = std::move(fn)](const RoutineContext &, std::any &in) -> std::any {
      auto resume =
          std::move(std::any_cast<gut::ntuple<CoIdType, InType, Resume> &>(in));
      std::any result =
          fn(std::move(resume.get(std::move(gut::type_v<InType>))));
      return resume.get(gut::type_v<Resume>).continuation(std::move(result));
    };
  }

  std::any run(std::any, ProgressPolicy);
  std::any run(Split);

  template <typename InType> std::any run(InType in) {
    auto pool = run(std::any{std::move(in)}, ProgressPolicy::EAGER_TO_YIELD);
    if (pool.type() == typeid(Yield))
      return std::move(std::any_cast<Yield &>(pool).value);
    return pool;
  }
};
} // namespace tonal
