#pragma once

#include <variant.hpp>

#include <deque>
#include <memory>

namespace tonal {
struct ImportStack {
  std::deque<gut::variant<
      std::shared_ptr<struct Module>, std::shared_ptr<struct Class>,
      std::shared_ptr<struct Function>, std::shared_ptr<struct Variable>>>
      import_stack;

  void push_import(std::shared_ptr<struct Module> import);
  void push_import(std::shared_ptr<struct Class> import);
  void push_import(std::shared_ptr<struct Function> import);
  void push_import(std::shared_ptr<struct Variable> import);

  void pop_import();

  gut::variant<std::shared_ptr<struct Module>, std::shared_ptr<struct Class>,
               std::shared_ptr<struct Function>,
               std::shared_ptr<struct Variable>> &
  top_entity();
  const gut::variant<
      std::shared_ptr<struct Module>, std::shared_ptr<struct Class>,
      std::shared_ptr<struct Function>, std::shared_ptr<struct Variable>> &
  top_entity() const;
};
} // namespace tonal
