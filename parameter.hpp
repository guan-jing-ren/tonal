#pragma once

#include "evaluation.hpp"
#include "token.hpp"

namespace tonal {
struct Parameter {
  Token token;
  Evaluation evaluation;

  bool is_variadic() const;

  bool is_fully_defined() const;
};
} // namespace tonal
