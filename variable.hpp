#pragma once

#include "entity.hpp"

namespace tonal {
struct Variable : Entity {
  Evaluation evaluation;

  bool is_fully_defined() const;

  void set_encloser(std::weak_ptr<Module> encloser);
  void set_encloser(std::weak_ptr<Class> encloser);
  void set_encloser(std::weak_ptr<Function> encloser);
};
} // namespace tonal
