#include "name.hpp"

#include <ostream>

namespace tonal {
std::ostream &operator<<(std::ostream &out, const Name &name) {
  return (name.parent.expired() ? out : out << *name.parent.lock() << '.')
         << *name.token;
}
} // namespace tonal
