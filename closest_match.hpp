#pragma once

#include <bitfield.hpp>

#include <iosfwd>

namespace tonal {
struct ClosestMatch {
  gut::bitfield<7, // [0] equal value
                6, // [1] 2 = equal type, 1 = compatible type
                4, // [2] 2 = param substitutable, 1 = param unspecified
                2, // [3] 2 = equal name, 1 = name unspecified
                0>
      match_flags{0b1, 0b10, 0, 0};

  bool equal_value() const;
  bool equal_type() const;
  bool equal_value_and_type() const;
  bool compatible_type() const;
  bool equal_value_and_compatible_type() const;
  bool incompatible() const;
  bool param_substitutable() const;
  bool param_unspecified() const;
  bool equal_name() const;
  bool name_unspecified() const;
  bool param_name_substitutable() const;
  bool substitutable() const;
  bool not_substitutable() const;

  void set_unequal_value();
  void set_compatible_type();
  void set_incompatible_type();
  void set_param_unspecified();
  void set_name_unspecified();
  void set_unsubstitutable();

  bool operator<(const ClosestMatch &that) const;
  bool operator>(const ClosestMatch &that) const;
};
std::ostream &operator<<(std::ostream &, const ClosestMatch &);
} // namespace tonal
