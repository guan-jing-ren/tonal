#pragma once

#include "entity.hpp"

namespace tonal {
struct Class : Entity {
  std::deque<std::weak_ptr<Class>> base_classes;
  std::deque<std::weak_ptr<Variable>> captures;

  std::deque<std::shared_ptr<Variable>> member_variables;
  std::deque<std::shared_ptr<Function>> member_functions;
  std::deque<std::shared_ptr<Class>> member_classes;

  void set_encloser(std::weak_ptr<Module> encloser);
  void set_encloser(std::weak_ptr<Class> encloser);
  void set_encloser(std::weak_ptr<Function> encloser);

  void add_member(std::shared_ptr<Class> member);
  void add_member(std::shared_ptr<Function> member);
  void add_member(std::shared_ptr<Variable> member);
};
} // namespace tonal