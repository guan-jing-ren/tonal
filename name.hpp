#pragma once

#include "token.hpp"

namespace tonal {
struct Name {
  Token token;
  std::weak_ptr<const Name> parent;
};

std::ostream &operator<<(std::ostream &, const Name &);
} // namespace tonal
