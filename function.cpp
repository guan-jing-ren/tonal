#include "function.hpp"

using namespace std;

namespace tonal {
bool Function::is_fully_defined() const {
  return Entity::is_fully_defined() && evaluation.is_fully_defined();
}

void Function::set_encloser(std::weak_ptr<Module> encloser) {
  this->encloser = encloser;
}

void Function::set_encloser(std::weak_ptr<Class> encloser) {
  this->encloser = encloser;
}

void Function::set_encloser(std::weak_ptr<Function> encloser) {
  this->encloser = encloser;
}

void Function::add_symbol(std::shared_ptr<Class> symbol) {
  symbols.emplace_back(move(symbol));
}

void Function::add_symbol(std::shared_ptr<Function> symbol) {
  symbols.emplace_back(move(symbol));
}

void Function::add_symbol(std::shared_ptr<Variable> symbol) {
  symbols.emplace_back(move(symbol));
}
} // namespace tonal
