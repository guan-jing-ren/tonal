#pragma once

#include "token.hpp"

#include <variant.hpp>

#include <deque>
#include <memory>

namespace tonal {
struct Evaluation {
  gut::optional<
      gut::variant<std::weak_ptr<struct Class>, std::weak_ptr<struct Function>,
                   std::weak_ptr<struct Variable>>>
      type;
  using byte_literal = std::deque<std::byte>;
  using value_type =
      gut::variant<int64_t, long double, byte_literal, Token,
                   std::weak_ptr<struct Class>, std::weak_ptr<struct Function>,
                   std::weak_ptr<struct Variable>, std::unique_ptr<struct List>,
                   std::deque<int64_t>, std::deque<long double>,
                   std::deque<byte_literal>>;
  gut::optional<value_type> value;

  Evaluation() = default;
  Evaluation(const Evaluation &) = delete;
  Evaluation(Evaluation &&) = default;
  Evaluation &operator=(const Evaluation &) = delete;
  Evaluation &operator=(Evaluation &&) = default;

  Evaluation replicate() const;

  bool is_fully_defined() const;
};
} // namespace tonal
