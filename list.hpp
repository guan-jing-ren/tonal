#pragma once

#include "evaluation.hpp"

#include <variant.hpp>

#include <memory>

namespace tonal {
struct ListCell {
  gut::variant<Token, std::shared_ptr<const struct Name>, Evaluation,
               std::unique_ptr<List>>
      value;

  ListCell replicate() const;
};

struct List {
  Token label;
  std::deque<ListCell> cells;

  static std::deque<ListCell>::iterator
  expand_into(std::unique_ptr<List> list, std::deque<ListCell> &expanded,
              std::deque<ListCell>::iterator insert_point);

  ~List();

  std::unique_ptr<List> replicate() const;
};
} // namespace tonal
