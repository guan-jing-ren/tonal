#include "evaluation.hpp"

#include "list.hpp"

using namespace std;
using namespace gut;

namespace tonal {

Evaluation Evaluation::replicate() const {
  Evaluation replicated;
  replicated.type = type;
  replicated.value = value.map([](auto &&value) {
    return move(value
                    .map([](auto &&value) {
                      constexpr gut::type value_type =
                          type_v<decltype(value)>.remove_cvref();
                      if constexpr (value_type.is_copy_constructible())
                        return value;
                      else
                        return value->replicate();
                    })
                    .value());
  });
  return replicated;
}

bool Evaluation::is_fully_defined() const { return type || value; }
} // namespace tonal
