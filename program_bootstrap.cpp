#include "program_bootstrap.hpp"

#include "evaluation.hpp"
#include "function.hpp"
#include "list.hpp"
#include "name.hpp"
#include "parameter.hpp"

#include <functor.hpp>

#include <iomanip>
#include <iostream>
#include <numeric>
#include <string>

using namespace std;
using namespace gut;

std::string to_string(const deque<byte> &b);
std::string to_string(const unique_ptr<tonal::List> &b);
namespace tonal {
deque<byte> to_bytes(const string_view &s);

Program::Bootstrap::Bootstrap(Program &program) {
  auto reserved = program.start_module().lock();
  program.give_entity_name("reserved");
  program.give_special_module_members();
  give_integer_functions(program);
  program.pend_entity();
  give_real_functions(program);
  program.pend_entity();
  give_list_functions(program);
  program.pend_entity();
  give_string_functions(program);
  program.pend_entity();
  program.pend_entity();
  program.import_state.push_import(reserved);
}

static void throw_missing_instance() {
  throw runtime_error{
      "Instance object missing when calling instance member function."};
}

static void throw_wrong_number_of_arguments() {
  throw runtime_error{"Wrong number of arguments provided."};
}

static void throw_unequal_zip_list_size() {
  throw runtime_error{"Unequal size of lists for zip operation."};
}

static void throw_missing_argument_value() {
  throw runtime_error{"Function argument missing value."};
}

static void throw_list_insert_position_out_of_range() {
  throw runtime_error{"List insert position out of range."};
}

static void throw_index_list_generation_out_of_range() {
  throw runtime_error{"Index list generation out of range."};
}

static void throw_list_partition_start_out_of_range() {
  throw runtime_error{"List partitition start out of range."};
}

static void throw_list_partition_count_out_of_range() {
  throw runtime_error{"List partitition count out of range."};
}

static void throw_negative_string_repeat() {
  throw runtime_error{"Negative value given for string repetition."};
}

static auto make_internal = [](auto arity_check, auto process_args) {
  return [arity_check, process_args](Evaluation &eval, Evaluation &instance,
                                     const deque<Parameter> &args) {
    if (!instance.value)
      throw_missing_instance();
    if (!arity_check(args.size()))
      throw_wrong_number_of_arguments();
    eval.type = instance.type;
    auto processor = process_args;
    eval.value = {Evaluation::value_type{processor(instance, args)}};
  };
};

static auto make_cumulative = [](auto value_type, auto operation,
                                 auto arity_check, auto accumulator,
                                 auto early_exit) {
  return make_internal(arity_check, [value_type, operation, accumulator,
                                     early_exit](auto &&instance,
                                                 auto &&args) mutable {
    const auto *prev = &instance.value.value().value(value_type).value();
    constexpr type result_type =
        type_v<decltype(operation(*prev, *prev))>.remove_cvref();
    gut::optional<typename decltype(result_type)::raw_type> result;
    if (args.empty())
      result = operation(*prev, *prev);
    for (auto &&arg : args)
      if (!arg.evaluation.value)
        throw_missing_argument_value();
      else if (auto equal = arg.evaluation.value.value().reduce(
                   [value_type, &operation, &prev,
                    result_type](auto &&value) mutable {
                     if constexpr (auto vtype =
                                       type_v<decltype(value)>.remove_cvref();
                                   vtype == value_type) {
                       auto result = operation(*prev, value);
                       prev = &value;
                       return result;
                     } else
                       return result_type.construct();
                   });
               early_exit(equal.value())) {
        result = accumulator(result, equal.value());
        break;
      } else
        result = accumulator(result, equal.value());
    return move(result.value());
  });
};

static auto make_accumulating_bool = [](auto value_type,
                                        auto operation) mutable {
  return make_cumulative(
      value_type, operation, arg_v > 0u,
      [](auto &&accum, auto &&result) mutable {
        if (!accum)
          return static_cast<int64_t>(result);
        return static_cast<int64_t>(accum.value() && result);
      },
      [](auto &&result) { return static_cast<int64_t>(!result); });
};

static auto make_accumulating = [](auto value_type, auto operation) mutable {
  auto op_wrapper = [operation](auto &&prev, auto &&next) mutable {
    if (!prev)
      return next;
    return operation(prev.value(), next);
  };
  return make_cumulative(
      value_type,
      [first = true, operation](auto &&prev, auto &&next) mutable {
        if (!first)
          return next;
        first = false;
        return operation(prev, next);
      },
      arg_v > 0u, op_wrapper, [](auto &&) { return static_cast<int64_t>(0); });
};

static auto make_equality = [](auto value_type) mutable {
  return make_accumulating_bool(value_type, arg_v == arg_v);
};

static auto make_weak_ordering = [](auto value_type) mutable {
  return make_accumulating_bool(value_type, arg_v < arg_v);
};

static auto make_binary = [](auto value_type, auto operation) mutable {
  return make_cumulative(value_type, operation, arg_v == 1u,
                         [](auto &&, auto &&right) { return move(right); },
                         [](auto &&) { return static_cast<int64_t>(0); });
};

static auto make_unary = [](auto value_type, auto operation) mutable {
  return make_cumulative(
      value_type, [operation](auto &&left, auto &&) { return operation(left); },
      arg_v == 0u, [](auto &&, auto &&right) { return move(right); },
      [](auto &&) { return static_cast<int64_t>(0); });
};

static auto make_heterogeneous = [](auto instance_type, auto arg_types,
                                    auto operation) mutable {
  return make_internal(
      arg_v == static_cast<size_t>(arg_types.count()),
      [arg_types, instance_type,
       operation](auto &&instance, auto &&args) mutable -> decltype(auto) {
        return arg_types.reduce(
            [instance_type, &operation, &instance,
             &args](auto... arg_types) mutable -> decltype(auto) {
              return operation(
                  instance.value.value().value(instance_type).value(),
                  args.at(get_index(arg_types))
                      .evaluation.value.value()
                      .value(get_type(arg_types))
                      .value()...);
            });
      });
};

static auto test_function = [](auto &&fn, auto &&type, auto &&self,
                               auto self_value, const deque<Parameter> &args) {
  cout << "Testing function: " << *fn->name.lock() << '\n';
  Evaluation return_val;
  Evaluation this_val{{{self}}, {type.cast(move(self_value))}};
  fn->internal(return_val, this_val, args);
  cout << "This: " << setprecision(15)
       << to_string(this_val.value.value().value(type).value()) << '\n';
  return_val.value.value().each([](auto &&value) {
    if constexpr (auto rtype = type_v<decltype(value)>.remove_cvref();
                  rtype == type_v<int64_t> || rtype == type_v<long double> ||
                  rtype == type_v<deque<byte>> ||
                  rtype == type_v<unique_ptr<List>>) {
      cout << "Return: " << setprecision(15) << to_string(value);
      if constexpr (rtype == type_v<int64_t>)
        cout << 'L';
      else if constexpr (rtype == type_v<long double>)
        cout << 'F';
      cout << '\n';
    }
  });
  return return_val;
};

void Program::Bootstrap::give_archetype_functions(
    Program &program, std::function<void(Evaluation &, Evaluation &,
                                         const std::deque<Parameter> &)>
                          equality) {
  auto self =
      program.define_state.value().value(type_v<shared_ptr<Class>>).value();
  program.give_special_class_members();
  program.start_function();
  program.give_entity_name("==");
  program.add_param_name("...comparands");
  program.give_param_evaluation({{weak_ptr<Class>{self}}, {}});
  program.give_evaluation({{{integer}}, {}});
  program.give_internal(move(equality));
  program.give_special_function_variables();
  program.pend_entity();
}

void Program::Bootstrap::give_compare_functions(
    Program &program,
    std::function<void(Evaluation &, Evaluation &,
                       const std::deque<Parameter> &)>
        equality,
    std::function<void(Evaluation &, Evaluation &,
                       const std::deque<Parameter> &)>
        weak_order) {
  auto self =
      program.define_state.value().value(type_v<shared_ptr<Class>>).value();
  give_archetype_functions(program, move(equality));
  program.start_function();
  program.give_entity_name("<");
  program.add_param_name("...comparands");
  program.give_param_evaluation({{weak_ptr<Class>{self}}, {}});
  program.give_evaluation({{{integer}}, {}});
  program.give_internal(move(weak_order));
  program.give_special_function_variables();
  program.pend_entity();
}

void Program::Bootstrap::give_container_functions(
    Program &program,
    std::function<void(Evaluation &, Evaluation &,
                       const std::deque<Parameter> &)>
        equality,
    std::function<void(Evaluation &, Evaluation &,
                       const std::deque<Parameter> &)>
        weak_order,
    std::function<void(Evaluation &, Evaluation &,
                       const std::deque<Parameter> &)>
        count,
    std::function<void(Evaluation &, Evaluation &,
                       const std::deque<Parameter> &)>
        contains) {
  auto self =
      program.define_state.value().value(type_v<shared_ptr<Class>>).value();
  give_compare_functions(program, move(equality), move(weak_order));
  program.start_function();
  program.give_entity_name("count");
  program.give_evaluation({{{integer}}, {}});
  program.give_internal(move(count));
  program.give_special_function_variables();
  program.pend_entity();
  program.start_function();
  program.give_entity_name("contains");
  program.add_param_name("...needles");
  program.give_param_evaluation({{weak_ptr<Class>{self}}, {}});
  program.give_evaluation({{{integer}}, {}});
  program.give_internal(move(contains));
  program.give_special_function_variables();
  program.pend_entity();
}

void Program::Bootstrap::give_list_functions(Program &program) {
  list = program.start_class().lock();
  program.give_entity_name("list");
  give_container_functions(
      program,
      make_binary(type_v<unique_ptr<List>>,
                  [](const unique_ptr<List> & /* left */,
                     const unique_ptr<List> & /* right */) { return false; }),
      make_binary(type_v<unique_ptr<List>>,
                  [](const unique_ptr<List> & /* left */,
                     const unique_ptr<List> & /* right */) { return false; }),
      make_unary(type_v<unique_ptr<List>>,
                 [](const unique_ptr<List> &self) {
                   return static_cast<int64_t>(self->cells.size());
                 }),
      [](auto &&...) { return false; });
  auto test_fn = program.start_function().lock();
  program.give_entity_name("list"); // .#.
  program.add_param_name("iota");
  program.give_param_evaluation({{{integer}}, {}});
  program.give_evaluation({{{list}}, {}});
  program.give_internal(make_heterogeneous(
      type_v<unique_ptr<List>>, make_type_list<int64_t>(),
      [this](const unique_ptr<List> &, int64_t iota) {
        if (iota < 0)
          throw_index_list_generation_out_of_range();
        auto list = make_unique<List>();
        generate_n(back_inserter(list->cells), iota,
                   [this, i = int64_t{0}]() mutable {
                     return ListCell{Evaluation{{integer}, {i++}}};
                   });
        return list;
      }));
  deque<Parameter> params;
  params.emplace_back(Parameter{{}, {{list}, {0LL}}});
  test_function(test_fn, type_v<unique_ptr<List>>, list, make_unique<List>(),
                {params});
  params.back().evaluation.value.value() = 15LL;
  test_function(test_fn, type_v<unique_ptr<List>>, list, make_unique<List>(),
                {params});
  program.give_special_function_variables();
  program.pend_entity();
  test_fn = program.start_function().lock();
  program.give_entity_name("...");
  program.add_param_name("position");
  program.give_param_evaluation({{{integer}}, {}});
  program.add_param_name("destination");
  program.give_param_evaluation({{{list}}, {}});
  program.give_evaluation({{{list}}, {}});
  program.give_internal(make_heterogeneous(
      type_v<unique_ptr<List>>, make_type_list<int64_t, unique_ptr<List>>(),
      [](const unique_ptr<List> &self, int64_t position,
         const unique_ptr<List> &destination) {
        if (position < 0 ||
            position > static_cast<int64_t>(destination->cells.size()))
          throw_list_insert_position_out_of_range();
        auto list = destination->replicate();
        List::expand_into(self->replicate(), list->cells,
                          list->cells.begin() + position);
        return list;
      }));
  params.clear();
  params.emplace_back(Parameter{{}, {{integer}, {0LL}}});
  auto dest_list = make_unique<List>();
  params.emplace_back(Parameter{{}, {{list}, {move(dest_list)}}});
  test_function(test_fn, type_v<unique_ptr<List>>, list, make_unique<List>(),
                {params});
  dest_list = make_unique<List>();
  dest_list->cells.push_back({Evaluation{{integer}, {0LL}}});
  params.back().evaluation.value.value() = move(dest_list);
  test_function(test_fn, type_v<unique_ptr<List>>, list, make_unique<List>(),
                {params});
  dest_list = make_unique<List>();
  dest_list->cells.push_back({Evaluation{{integer}, {0LL}}});
  params.front().evaluation.value.value() = 1LL;
  params.back().evaluation.value.value() = move(dest_list);
  test_function(test_fn, type_v<unique_ptr<List>>, list, make_unique<List>(),
                {params});
  deque<Parameter> empty_list_param;
  empty_list_param.emplace_back(Parameter{{}, {{integer}, {0LL}}});
  empty_list_param.emplace_back(Parameter{{}, {{list}, {make_unique<List>()}}});
  auto src_list = make_unique<List>();
  src_list->cells.push_back({Evaluation{{integer}, {101LL}}});
  src_list->cells.push_back({Evaluation{{integer}, {102LL}}});
  src_list->cells.push_back({Evaluation{{integer}, {103LL}}});
  test_function(test_fn, type_v<unique_ptr<List>>, list, src_list->replicate(),
                {empty_list_param});
  test_function(test_fn, type_v<unique_ptr<List>>, list, src_list->replicate(),
                {params});
  dest_list = make_unique<List>();
  dest_list->cells.push_back({Evaluation{{integer}, {0LL}}});
  params.front().evaluation.value.value() = 0LL;
  params.back().evaluation.value.value() = dest_list->replicate();
  test_function(test_fn, type_v<unique_ptr<List>>, list, src_list->replicate(),
                {params});
  dest_list->cells.push_back({Evaluation{{integer}, {1LL}}});
  params.front().evaluation.value.value() = 1LL;
  params.back().evaluation.value.value() = dest_list->replicate();
  test_function(test_fn, type_v<unique_ptr<List>>, list, src_list->replicate(),
                {params});
  program.give_special_function_variables();
  program.pend_entity();
  test_fn = program.start_function().lock();
  program.give_entity_name(".|.");
  program.add_param_name("start");
  program.give_param_evaluation({{{integer}}, {}});
  program.add_param_name("count");
  program.give_param_evaluation({{{integer}}, {}});
  program.give_evaluation({{{list}}, {}});
  program.give_internal(make_heterogeneous(
      type_v<unique_ptr<List>>, make_type_list<int64_t, int64_t>(),
      [](const unique_ptr<List> &self, int64_t start, int64_t count) {
        if (start < 0 || start > static_cast<int64_t>(self->cells.size()))
          throw_list_partition_start_out_of_range();
        if (count < 0 ||
            (start + count) > static_cast<int64_t>(self->cells.size()))
          throw_list_partition_count_out_of_range();
        auto list = self->replicate();
        list->cells.erase(list->cells.begin(), list->cells.begin() + start);
        list->cells.erase(list->cells.begin() + count, list->cells.end());
        return list;
      }));
  src_list->cells.push_back({Evaluation{{integer}, {104LL}}});
  src_list->cells.push_back({Evaluation{{integer}, {105LL}}});
  src_list->cells.push_back({Evaluation{{integer}, {106LL}}});
  params.clear();
  params.push_back({{}, Evaluation{{integer}, {0LL}}});
  params.push_back({{}, Evaluation{{integer}, {0LL}}});
  test_function(test_fn, type_v<unique_ptr<List>>, list, src_list->replicate(),
                {params});
  params.front().evaluation.value.value() = 0LL;
  params.back().evaluation.value.value() = 1LL;
  test_function(test_fn, type_v<unique_ptr<List>>, list, src_list->replicate(),
                {params});
  params.front().evaluation.value.value() = 5LL;
  params.back().evaluation.value.value() = 1LL;
  test_function(test_fn, type_v<unique_ptr<List>>, list, src_list->replicate(),
                {params});
  params.front().evaluation.value.value() = 2LL;
  params.back().evaluation.value.value() = 2LL;
  test_function(test_fn, type_v<unique_ptr<List>>, list, src_list->replicate(),
                {params});
  params.front().evaluation.value.value() = 0LL;
  params.back().evaluation.value.value() = 2LL;
  test_function(test_fn, type_v<unique_ptr<List>>, list, src_list->replicate(),
                {params});
  params.front().evaluation.value.value() = 0LL;
  params.back().evaluation.value.value() = 6LL;
  test_function(test_fn, type_v<unique_ptr<List>>, list, src_list->replicate(),
                {params});
  program.give_special_function_variables();
  program.pend_entity();
  test_fn = program.start_function().lock();
  auto reverse_fn = test_fn;
  program.give_entity_name(".~.");
  program.give_evaluation({{{list}}, {}});
  program.give_internal(
      make_unary(type_v<unique_ptr<List>>, [](const unique_ptr<List> &self) {
        auto replicated = self->replicate();
        reverse(replicated->cells.begin(), replicated->cells.end());
        return replicated;
      }));
  test_function(test_fn, type_v<unique_ptr<List>>, list, make_unique<List>(),
                {});
  test_function(test_fn, type_v<unique_ptr<List>>, list, src_list->replicate(),
                {});
  program.give_special_function_variables();
  program.pend_entity();
  program.start_function();
  program.give_entity_name(".*.");
  program.add_param_name("transformation");
  program.give_evaluation({{{list}}, {}});
  program.give_special_function_variables();
  program.pend_entity();
  test_fn = program.start_function().lock();
  program.give_entity_name(".#.");
  program.give_evaluation({{{list}}, {}});
  program.give_internal(make_unary(
      type_v<unique_ptr<List>>, [this](const unique_ptr<List> &object) {
        auto list_enum = make_unique<List>();
        transform(object->cells.cbegin(), object->cells.cend(),
                  back_inserter(list_enum->cells),
                  [this, i = count_t{0}](auto &&cell) mutable {
                    auto element = make_unique<List>();
                    element->cells.emplace_back(
                        ListCell{Evaluation{{integer}, {i++}}});
                    element->cells.emplace_back(cell.replicate());
                    return ListCell{Evaluation{{list}, {move(element)}}};
                  });
        return list_enum;
      }));
  test_function(test_fn, type_v<unique_ptr<List>>, list, make_unique<List>(),
                {});
  test_function(test_fn, type_v<unique_ptr<List>>, list, src_list->replicate(),
                {});
  program.give_special_function_variables();
  program.pend_entity();
  test_fn = program.start_function().lock();
  program.give_entity_name(".$.");
  program.add_param_name("ordinate");
  program.give_param_evaluation({{{list}}, {}});
  program.give_evaluation({{{list}}, {}});
  program.give_internal(make_binary(
      type_v<unique_ptr<List>>,
      [this](const unique_ptr<List> &object, const unique_ptr<List> &ordinate) {
        if (object->cells.size() != ordinate->cells.size())
          throw_unequal_zip_list_size();
        return inner_product(
            object->cells.cbegin(), object->cells.cend(),
            ordinate->cells.cbegin(), make_unique<List>(),
            [](auto &&list, auto &&product) {
              list->cells.emplace_back(move(product));
              return move(list);
            },
            [this](auto &&left, auto &&right) mutable {
              auto element = make_unique<List>();
              element->cells.emplace_back(left.replicate());
              element->cells.emplace_back(right.replicate());
              return ListCell{Evaluation{{list}, {move(element)}}};
            });
      }));
  auto ord_list = move(test_function(reverse_fn, type_v<unique_ptr<List>>, list,
                                     src_list->replicate(), {})
                           .value.value()
                           .value(type_v<unique_ptr<List>>)
                           .value());
  params.clear();
  params.push_back({{}, Evaluation{{list}, {make_unique<List>()}}});
  test_function(test_fn, type_v<unique_ptr<List>>, list, make_unique<List>(),
                {params});
  params.back().evaluation.value.value() = ord_list->replicate();
  test_function(test_fn, type_v<unique_ptr<List>>, list, src_list->replicate(),
                {params});
  program.give_special_function_variables();
  program.pend_entity();
}

void Program::Bootstrap::give_string_functions(Program &program) {
  string = program.start_class().lock();
  program.give_entity_name("string");
  give_container_functions(
      program, make_equality(type_v<deque<byte>>),
      make_weak_ordering(type_v<deque<byte>>),
      make_unary(type_v<deque<byte>>,
                 [](const deque<byte> &self) {
                   return static_cast<int64_t>(self.size());
                 }),
      make_accumulating_bool(
          type_v<deque<byte>>,
          [self = gut::optional<deque<byte>>{}](
              const deque<byte> &left, const deque<byte> &right) mutable {
            if (!self)
              self = left;
            return static_cast<int64_t>(
                search(self.value().cbegin(), self.value().cend(),
                       right.cbegin(), right.cend()) != self.value().cend());
          }));
  auto test_fn = program.start_function().lock();
  program.give_entity_name("+");
  program.add_param_name("...summands");
  program.give_param_evaluation({{{string}}, {}});
  program.give_evaluation({{{string}}, {}});
  program.give_internal(
      make_accumulating(type_v<deque<byte>>, [](const deque<byte> &left,
                                                const deque<byte> &right) {
        auto result = left;
        result.insert(result.end(), right.cbegin(), right.cend());
        return result;
      }));
  deque<Parameter> params;
  params.emplace_back(Parameter{{}, {{string}, {to_bytes("b")}}});
  params.emplace_back(Parameter{{}, {{string}, {to_bytes("c")}}});
  params.emplace_back(Parameter{{}, {{string}, {to_bytes("d")}}});
  params.emplace_back(Parameter{{}, {{string}, {to_bytes("e")}}});
  params.emplace_back(Parameter{{}, {{string}, {to_bytes("f")}}});
  params.emplace_back(Parameter{{}, {{string}, {to_bytes("g")}}});
  params.emplace_back(Parameter{{}, {{string}, {to_bytes("h")}}});
  params.emplace_back(Parameter{{}, {{string}, {to_bytes("i")}}});
  params.emplace_back(Parameter{{}, {{string}, {to_bytes("j")}}});
  params.emplace_back(Parameter{{}, {{string}, {to_bytes("k")}}});
  params.emplace_back(Parameter{{}, {{string}, {to_bytes("l")}}});
  params.emplace_back(Parameter{{}, {{string}, {to_bytes("m")}}});
  test_function(test_fn, type_v<deque<byte>>, string, to_bytes("a"), {params});
  test_function(test_fn, type_v<deque<byte>>, string, to_bytes("a"), {params});
  program.give_special_function_variables();
  program.pend_entity();
  test_fn = program.start_function().lock();
  program.give_entity_name("*");
  program.add_param_name("repeat");
  program.give_param_evaluation({{{integer}}, {}});
  program.give_evaluation({{{string}}, {}});
  program.give_internal(
      make_heterogeneous(type_v<deque<byte>>, make_type_list<int64_t>(),
                         [](const deque<byte> &self, int64_t repeat) {
                           if (repeat < 0)
                             throw_negative_string_repeat();
                           deque<byte> rep;
                           while (repeat--)
                             copy(cbegin(self), cend(self), back_inserter(rep));
                           return rep;
                         }));
  params.clear();
  params.emplace_back(Parameter{{}, {{integer}, {0LL}}});
  test_function(test_fn, type_v<deque<byte>>, string, to_bytes("a"), {params});
  params.back().evaluation.value.value() = 1LL;
  test_function(test_fn, type_v<deque<byte>>, string, to_bytes("a"), {params});
  params.back().evaluation.value.value() = 5LL;
  test_function(test_fn, type_v<deque<byte>>, string, to_bytes("a"), {params});
  program.give_special_function_variables();
  program.pend_entity();
  test_fn = program.start_function().lock();
  program.give_entity_name("split");
  program.add_param_name("separator");
  program.give_param_evaluation({{{string}}, {}});
  program.give_evaluation({{{list}}, {}});
  program.give_internal(
      make_binary(type_v<deque<byte>>, [this](const deque<byte> &left,
                                              const deque<byte> &right) {
        auto list = make_unique<List>();
        for (auto first = left.cbegin(), last = left.cbegin();
             last != left.cend(); first = last + right.size()) {
          last = search(first, left.cend(), right.cbegin(), right.cend()) +
                 right.empty();
          list->cells.push_back(
              {Evaluation{{{string}}, {{deque<byte>{first, last}}}}});
        }
        return list;
      }));
  params.clear();
  params.emplace_back(Parameter{{}, {{string}, {to_bytes(",")}}});
  test_function(test_fn, type_v<deque<byte>>, string,
                to_bytes("a,b,c,d,e,f,g,h,i,j,k"), {params});
  test_function(test_fn, type_v<deque<byte>>, string,
                to_bytes("a,b,c,d,e,f,g,h,i,j,k,"), {params});
  test_function(test_fn, type_v<deque<byte>>, string,
                to_bytes(",a,b,c,d,e,f,g,h,i,j,k"), {params});
  test_function(test_fn, type_v<deque<byte>>, string,
                to_bytes(",a,b,c,d,e,f,g,h,i,j,k,"), {params});
  test_function(test_fn, type_v<deque<byte>>, string,
                to_bytes(",a,b,c,d,e,f,,g,h,i,j,k,"), {params});
  test_function(test_fn, type_v<deque<byte>>, string,
                to_bytes(",,a,b,c,d,e,f,,g,h,i,j,k,"), {params});
  test_function(test_fn, type_v<deque<byte>>, string,
                to_bytes(",a,b,c,d,e,f,,g,h,i,j,k,,"), {params});
  test_function(test_fn, type_v<deque<byte>>, string,
                to_bytes(",,a,b,c,d,e,f,,g,h,i,j,k,,"), {params});
  test_function(test_fn, type_v<deque<byte>>, string, to_bytes(",,"), {params});
  test_function(test_fn, type_v<deque<byte>>, string, to_bytes(","), {params});
  test_function(test_fn, type_v<deque<byte>>, string, to_bytes(""), {params});
  params.back().evaluation.value.value() = to_bytes("");
  test_function(test_fn, type_v<deque<byte>>, string, to_bytes("abcdefghijk"),
                {params});
  program.give_special_function_variables();
  program.pend_entity();
  auto prefix = [](const deque<byte> &left, const deque<byte> &right) {
    return static_cast<int64_t>(
        mismatch(left.cbegin(), left.cend(), right.cbegin(), right.cend())
            .second == right.cend());
  };
  auto suffix = [](const deque<byte> &left, const deque<byte> &right) {
    return static_cast<int64_t>(
        mismatch(left.crbegin(), left.crend(), right.crbegin(), right.crend())
            .second == right.crend());
  };
  test_fn = program.start_function().lock();
  program.give_entity_name("starts_with");
  program.add_param_name("prefix");
  program.give_param_evaluation({{{string}}, {}});
  program.give_evaluation({{{integer}}, {}});
  program.give_internal(make_binary(type_v<deque<byte>>, prefix));
  params.clear();
  params.emplace_back(Parameter{{}, {{string}, {to_bytes("abc")}}});
  test_function(test_fn, type_v<deque<byte>>, string, to_bytes("abcdefg"),
                {params});
  test_function(test_fn, type_v<deque<byte>>, string, to_bytes("abdcefg"),
                {params});
  program.give_special_function_variables();
  program.pend_entity();
  test_fn = program.start_function().lock();
  program.give_entity_name("ends_with");
  program.add_param_name("suffix");
  program.give_param_evaluation({{{string}}, {}});
  program.give_evaluation({{{integer}}, {}});
  program.give_internal(make_binary(type_v<deque<byte>>, suffix));
  params.clear();
  params.emplace_back(Parameter{{}, {{string}, {to_bytes("efg")}}});
  test_function(test_fn, type_v<deque<byte>>, string, to_bytes("abcdefg"),
                {params});
  test_function(test_fn, type_v<deque<byte>>, string, to_bytes("abcedfg"),
                {params});
  program.give_special_function_variables();
  program.pend_entity();
}

template <typename Type>
void Program::Bootstrap::give_number_functions(Program &program, Type type) {
  auto self =
      program.define_state.value().value(type_v<shared_ptr<Class>>).value();
  give_compare_functions(program, make_equality(type),
                         make_weak_ordering(type));
  auto test_fn = program.start_function().lock();
  program.give_entity_name("abs");
  program.give_evaluation({{{self}}, {}});
  program.give_internal(make_unary(type, [](auto self) { return abs(self); }));
  program.give_special_function_variables();
  test_function(test_fn, type, self, 12.42L, {});
  test_function(test_fn, type, self, -12.42L, {});
  program.pend_entity();
  test_fn = program.start_function().lock();
  program.give_entity_name("++");
  program.give_evaluation({{{self}}, {}});
  program.give_internal(make_unary(type, arg_v + 1));
  test_function(test_fn, type, self, 12.42L, {});
  test_function(test_fn, type, self, -12.42L, {});
  program.give_special_function_variables();
  program.pend_entity();
  test_fn = program.start_function().lock();
  program.give_entity_name("--");
  program.give_evaluation({{{self}}, {}});
  program.give_internal(make_unary(type, arg_v - 1));
  test_function(test_fn, type, self, 12.42L, {});
  test_function(test_fn, type, self, -12.42L, {});
  program.give_special_function_variables();
  program.pend_entity();
  test_fn = program.start_function().lock();
  program.give_entity_name("+");
  program.add_param_name("...summands");
  program.give_param_evaluation({{{self}}, {}});
  program.give_evaluation({{{self}}, {}});
  program.give_internal(make_accumulating(type, arg_v + arg_v));
  deque<Parameter> params;
  params.emplace_back(Parameter{{}, {{self}, {type.cast(1.1L)}}});
  params.emplace_back(Parameter{{}, {{self}, {type.cast(1.1L)}}});
  params.emplace_back(Parameter{{}, {{self}, {type.cast(1.1L)}}});
  params.emplace_back(Parameter{{}, {{self}, {type.cast(1.1L)}}});
  params.emplace_back(Parameter{{}, {{self}, {type.cast(1.1L)}}});
  params.emplace_back(Parameter{{}, {{self}, {type.cast(1.1L)}}});
  params.emplace_back(Parameter{{}, {{self}, {type.cast(1.1L)}}});
  params.emplace_back(Parameter{{}, {{self}, {type.cast(1.1L)}}});
  params.emplace_back(Parameter{{}, {{self}, {type.cast(1.1L)}}});
  params.emplace_back(Parameter{{}, {{self}, {type.cast(1.1L)}}});
  params.emplace_back(Parameter{{}, {{self}, {type.cast(1.1L)}}});
  params.emplace_back(Parameter{{}, {{self}, {type.cast(1.1L)}}});
  params.emplace_back(Parameter{{}, {{self}, {type.cast(1.1L)}}});
  test_function(test_fn, type, self, 0.L, {params});
  program.give_special_function_variables();
  program.pend_entity();
  test_fn = program.start_function().lock();
  program.give_entity_name("-");
  program.give_evaluation({{{self}}, {}});
  program.give_internal(make_unary(type, -arg_v));
  test_function(test_fn, type, self, 12.42L, {});
  test_function(test_fn, type, self, -12.42L, {});
  program.give_special_function_variables();
  program.pend_entity();
  test_fn = program.start_function().lock();
  program.give_entity_name("-");
  program.add_param_name("...subtrahends");
  program.give_param_evaluation({{{self}}, {}});
  program.give_evaluation({{{self}}, {}});
  program.give_internal(make_accumulating(type, arg_v - arg_v));
  test_function(test_fn, type, self, 0.L, {params});
  program.give_special_function_variables();
  program.pend_entity();
  test_fn = program.start_function().lock();
  program.give_entity_name("*");
  program.add_param_name("...multiplicands");
  program.give_param_evaluation({{{self}}, {}});
  program.give_evaluation({{{self}}, {}});
  program.give_internal(make_accumulating(type, arg_v * arg_v));
  test_function(test_fn, type, self, 1.L, {params});
  program.give_special_function_variables();
  program.pend_entity();
  test_fn = program.start_function().lock();
  program.give_entity_name("/");
  program.add_param_name("divisor");
  program.give_param_evaluation({{{self}}, {}});
  program.give_evaluation({{{self}}, {}});
  program.give_internal(make_binary(type, arg_v / arg_v));
  params.clear();
  params.emplace_back(Parameter{{}, {{self}, {type.cast(113.L)}}});
  test_function(test_fn, type, self, type.cast(355.L), {params});
  program.give_special_function_variables();
  program.pend_entity();
}

void Program::Bootstrap::give_integer_functions(Program &program) {
  integer = program.start_class().lock();
  program.give_entity_name("integer");
  give_number_functions(program, type_v<int64_t>);
  auto test_fn = program.start_function().lock();
  program.give_entity_name("%");
  program.add_param_name("divisor");
  program.give_param_evaluation({{{integer}}, {}});
  program.give_evaluation({{{integer}}, {}});
  program.give_internal(make_binary(type_v<int64_t>, arg_v % arg_v));
  deque<Parameter> params;
  params.emplace_back(Parameter{{}, {{integer}, {113LL}}});
  test_function(test_fn, type_v<int64_t>, integer, 355LL, {params});
  program.give_special_function_variables();
  program.pend_entity();
  test_fn = program.start_function().lock();
  program.give_entity_name("&");
  program.add_param_name("...conjuncts");
  program.give_param_evaluation({{{integer}}, {}});
  program.give_evaluation({{{integer}}, {}});
  program.give_internal(make_accumulating(type_v<int64_t>, arg_v & arg_v));
  params.clear();
  params.emplace_back(Parameter{{}, {{integer}, {0xFF'00'FF'FF'FFLL}}});
  params.emplace_back(Parameter{{}, {{integer}, {0xFF'FF'FF'00'FFLL}}});
  cout << hex;
  test_function(test_fn, type_v<int64_t>, integer, 0xFF'FF'FF'FF'FFLL,
                {params});
  cout << dec;
  program.give_special_function_variables();
  program.pend_entity();
  test_fn = program.start_function().lock();
  program.give_entity_name("|");
  program.add_param_name("...disjuncts");
  program.give_param_evaluation({{{integer}}, {}});
  program.give_evaluation({{{integer}}, {}});
  program.give_internal(make_accumulating(type_v<int64_t>, arg_v | arg_v));
  params.clear();
  params.emplace_back(Parameter{{}, {{integer}, {0xFF'00'00'00'00LL}}});
  params.emplace_back(Parameter{{}, {{integer}, {0x00'FF'00'00'00LL}}});
  params.emplace_back(Parameter{{}, {{integer}, {0x00'00'FF'00'00LL}}});
  params.emplace_back(Parameter{{}, {{integer}, {0x00'00'00'00'FFLL}}});
  cout << hex;
  test_function(test_fn, type_v<int64_t>, integer, 0x00'00'00'00'00LL,
                {params});
  cout << dec;
  program.give_special_function_variables();
  program.pend_entity();
  test_fn = program.start_function().lock();
  program.give_entity_name("~");
  program.give_evaluation({{{integer}}, {}});
  program.give_internal(make_unary(type_v<int64_t>, ~arg_v));
  cout << hex;
  test_function(test_fn, type_v<int64_t>, integer, 0LL, {});
  cout << dec;
  program.give_special_function_variables();
  program.pend_entity();
  test_fn = program.start_function().lock();
  program.give_entity_name("^");
  program.add_param_name("...exjuncts");
  program.give_param_evaluation({{{integer}}, {}});
  program.give_evaluation({{{integer}}, {}});
  program.give_internal(make_accumulating(type_v<int64_t>, arg_v ^ arg_v));
  program.give_special_function_variables();
  program.pend_entity();
  test_fn = program.start_function().lock();
  program.give_entity_name("<<");
  program.add_param_name("multiplicand");
  program.give_param_evaluation({{{integer}}, {}});
  program.give_evaluation({{{integer}}, {}});
  program.give_internal(make_binary(type_v<int64_t>, arg_v << arg_v));
  params.clear();
  params.emplace_back(Parameter{{}, {{integer}, {4LL}}});
  cout << hex;
  test_function(test_fn, type_v<int64_t>, integer, 0xFFLL, {params});
  program.give_special_function_variables();
  program.pend_entity();
  test_fn = program.start_function().lock();
  program.give_entity_name(">>");
  program.add_param_name("divisor");
  program.give_param_evaluation({{{integer}}, {}});
  program.give_evaluation({{{integer}}, {}});
  program.give_internal(make_binary(type_v<int64_t>, arg_v >> arg_v));
  params.clear();
  params.emplace_back(Parameter{{}, {{integer}, {4LL}}});
  cout << hex;
  test_function(test_fn, type_v<int64_t>, integer, 0xFF'00LL, {params});
  cout << dec;
  program.give_special_function_variables();
  program.pend_entity();
  test_fn = program.start_function().lock();
  program.give_entity_name("gcd");
  program.add_param_name("operand");
  program.give_param_evaluation({{{integer}}, {}});
  program.give_evaluation({{{integer}}, {}});
  program.give_internal(
      make_binary(type_v<int64_t>, [](int64_t self, int64_t operand) {
        return gcd(self, operand);
      }));
  params.clear();
  params.emplace_back(Parameter{{}, {{integer}, {9LL}}});
  test_function(test_fn, type_v<int64_t>, integer, 6LL, {params});
  program.give_special_function_variables();
  program.pend_entity();
  test_fn = program.start_function().lock();
  program.give_entity_name("lcm");
  program.add_param_name("operand");
  program.give_param_evaluation({{{integer}}, {}});
  program.give_evaluation({{{integer}}, {}});
  program.give_internal(
      make_binary(type_v<int64_t>, [](int64_t self, int64_t operand) {
        return lcm(self, operand);
      }));
  params.clear();
  params.emplace_back(Parameter{{}, {{integer}, {9LL}}});
  test_function(test_fn, type_v<int64_t>, integer, 6LL, {params});
  program.give_special_function_variables();
  program.pend_entity();
}

void Program::Bootstrap::give_real_functions(Program &program) {
  real = program.start_class().lock();
  program.give_entity_name("real");
  static const long double e = exp(1.L);
  static const long double pi = acos(-1.L);
  cout << setprecision(40);
  cout << "e: " << e << '\n';
  cout << "pi: " << pi << '\n';
  give_number_functions(program, type_v<long double>);
  auto test_fn = program.start_function().lock();
  program.give_entity_name("logarithm");
  program.add_param_name("base");
  program.give_param_evaluation({{{real}}, {}});
  program.give_evaluation({{{real}}, {}});
  program.give_internal(
      make_binary(type_v<long double>, [](long double left, long double right) {
        if (right == exp(1.L))
          return log(left);
        if (right == 2.L)
          return log2(left);
        if (right == 10.L)
          return log10(left);
        return log(left) / log(right);
      }));
  deque<Parameter> params;
  params.emplace_back(Parameter{{}, {{real}, {e}}});
  test_function(test_fn, type_v<long double>, real, exp(5.L), {params});
  params.clear();
  params.emplace_back(Parameter{{}, {{real}, {2.L}}});
  test_function(test_fn, type_v<long double>, real, pow(2.L, 10.L), {params});
  params.clear();
  params.emplace_back(Parameter{{}, {{real}, {10.L}}});
  test_function(test_fn, type_v<long double>, real, pow(10.L, 100.L), {params});
  params.clear();
  params.emplace_back(Parameter{{}, {{real}, {5.L}}});
  test_function(test_fn, type_v<long double>, real, 125.L, {params});
  program.give_special_function_variables();
  program.pend_entity();
  test_fn = program.start_function().lock();
  program.give_entity_name("power");
  program.add_param_name("exponent");
  program.give_param_evaluation({{{real}}, {}});
  program.give_evaluation({{{real}}, {}});
  program.give_internal(
      make_binary(type_v<long double>, [](long double left, long double right) {
        if (left == exp(1.L))
          return exp(right);
        if (left == 2.L)
          return exp2(right);
        if (right == .5L)
          return sqrt(left);
        if (right == 1.L / 3.L)
          return cbrt(left);
        return pow(left, right);
      }));
  params.clear();
  params.emplace_back(Parameter{{}, {{real}, {log(5.L)}}});
  test_function(test_fn, type_v<long double>, real, e, {params});
  params.clear();
  params.emplace_back(Parameter{{}, {{real}, {log2(10.L)}}});
  test_function(test_fn, type_v<long double>, real, 2.L, {params});
  params.clear();
  params.emplace_back(Parameter{{}, {{real}, {.5L}}});
  test_function(test_fn, type_v<long double>, real, 16.L, {params});
  params.clear();
  params.emplace_back(Parameter{{}, {{real}, {1.L / 3.L}}});
  test_function(test_fn, type_v<long double>, real, 27.L, {params});
  params.clear();
  params.emplace_back(Parameter{{}, {{real}, {100.L}}});
  test_function(test_fn, type_v<long double>, real, 10.L, {params});
  params.clear();
  params.emplace_back(Parameter{{}, {{real}, {1.L / 4.L}}});
  test_function(test_fn, type_v<long double>, real, 81.L, {params});
  program.give_special_function_variables();
  program.pend_entity();
  test_fn = program.start_function().lock();
  program.give_entity_name("hypotenuse");
  program.add_param_name("y");
  program.give_param_evaluation({{{real}}, {}});
  program.give_evaluation({{{real}}, {}});
  program.give_internal(
      make_binary(type_v<long double>, [](long double left, long double right) {
        return hypot(left, right);
      }));
  params.clear();
  params.emplace_back(Parameter{{}, {{real}, {4.L}}});
  test_function(test_fn, type_v<long double>, real, 3.L, {params});
  program.give_special_function_variables();
  program.pend_entity();
  test_fn = program.start_function().lock();
  program.give_entity_name("hypotenuse");
  program.add_param_name("y");
  program.give_param_evaluation({{{real}}, {}});
  program.add_param_name("z");
  program.give_param_evaluation({{{real}}, {}});
  program.give_evaluation({{{real}}, {}});
  program.give_internal(make_heterogeneous(
      type_v<long double>, make_type_list<long double, long double>(),
      [](long double left, long double y, long double z) {
        return hypot(left, y, z);
      }));
  params.clear();
  params.emplace_back(Parameter{{}, {{real}, {3.L}}});
  params.emplace_back(Parameter{{}, {{real}, {6.L}}});
  test_function(test_fn, type_v<long double>, real, 2.L, {params});
  program.give_special_function_variables();
  program.pend_entity();
  test_fn = program.start_function().lock();
  program.give_entity_name("sine");
  program.give_evaluation({{{real}}, {}});
  program.give_internal(make_unary(type_v<long double>,
                                   [](long double self) { return sin(self); }));
  test_function(test_fn, type_v<long double>, real, pi, {});
  test_function(test_fn, type_v<long double>, real, -pi, {});
  test_function(test_fn, type_v<long double>, real, pi / 2.L, {});
  test_function(test_fn, type_v<long double>, real, -pi / 2.L, {});
  program.give_special_function_variables();
  program.pend_entity();
  test_fn = program.start_function().lock();
  program.give_entity_name("cosine");
  program.give_evaluation({{{real}}, {}});
  program.give_internal(make_unary(type_v<long double>,
                                   [](long double self) { return cos(self); }));
  test_function(test_fn, type_v<long double>, real, pi, {});
  test_function(test_fn, type_v<long double>, real, -pi, {});
  test_function(test_fn, type_v<long double>, real, pi / 2.L, {});
  test_function(test_fn, type_v<long double>, real, -pi / 2.L, {});
  program.give_special_function_variables();
  program.pend_entity();
  test_fn = program.start_function().lock();
  program.give_entity_name("tangent");
  program.give_evaluation({{{real}}, {}});
  program.give_internal(make_unary(type_v<long double>,
                                   [](long double self) { return tan(self); }));
  test_function(test_fn, type_v<long double>, real, pi / 4.L, {});
  test_function(test_fn, type_v<long double>, real, -pi / 4.L, {});
  test_function(test_fn, type_v<long double>, real, pi * 3.L / 4.L, {});
  test_function(test_fn, type_v<long double>, real, -pi * 3.L / 4.L, {});
  program.give_special_function_variables();
  program.pend_entity();
  test_fn = program.start_function().lock();
  program.give_entity_name("arc-sine");
  program.give_evaluation({{{real}}, {}});
  program.give_internal(make_unary(
      type_v<long double>, [](long double self) { return asin(self); }));
  test_function(test_fn, type_v<long double>, real, 0.L, {});
  test_function(test_fn, type_v<long double>, real, 1.L, {});
  test_function(test_fn, type_v<long double>, real, -1.L, {});
  program.give_special_function_variables();
  program.pend_entity();
  test_fn = program.start_function().lock();
  program.give_entity_name("arc-cosine");
  program.give_evaluation({{{real}}, {}});
  program.give_internal(make_unary(
      type_v<long double>, [](long double self) { return acos(self); }));
  test_function(test_fn, type_v<long double>, real, 0.L, {});
  test_function(test_fn, type_v<long double>, real, 1.L, {});
  test_function(test_fn, type_v<long double>, real, -1.L, {});
  program.give_special_function_variables();
  program.pend_entity();
  test_fn = program.start_function().lock();
  program.give_entity_name("arc-tangent");
  program.give_evaluation({{{real}}, {}});
  program.give_internal(make_unary(
      type_v<long double>, [](long double self) { return atan(self); }));
  test_function(test_fn, type_v<long double>, real, 0.L, {});
  test_function(test_fn, type_v<long double>, real, 1.L, {});
  test_function(test_fn, type_v<long double>, real, -1.L, {});
  program.give_special_function_variables();
  program.pend_entity();
  test_fn = program.start_function().lock();
  program.give_entity_name("arc-tangent");
  program.add_param_name("y");
  program.give_param_evaluation({{{real}}, {}});
  program.give_evaluation({{{real}}, {}});
  program.give_internal(
      make_binary(type_v<long double>, [](long double self, long double y) {
        return atan2(self, y);
      }));
  params.clear();
  params.emplace_back(Parameter{{}, {{real}, {sin(pi / 4.L)}}});
  test_function(test_fn, type_v<long double>, real, cos(pi / 4.L), {params});
  params.clear();
  params.emplace_back(Parameter{{}, {{real}, {sin(pi / 4.L)}}});
  test_function(test_fn, type_v<long double>, real, -cos(pi / 4.L), {params});
  params.clear();
  params.emplace_back(Parameter{{}, {{real}, {-sin(pi / 4.L)}}});
  test_function(test_fn, type_v<long double>, real, cos(pi / 4.L), {params});
  params.clear();
  params.emplace_back(Parameter{{}, {{real}, {-sin(pi / 4.L)}}});
  test_function(test_fn, type_v<long double>, real, -cos(pi / 4.L), {params});
  program.give_special_function_variables();
  program.pend_entity();
  program.start_function();
  program.give_entity_name("hyperbolic-sin");
  program.give_evaluation({{{real}}, {}});
  program.give_internal(make_unary(
      type_v<long double>, [](long double self) { return sinh(self); }));
  program.give_special_function_variables();
  program.pend_entity();
  program.start_function();
  program.give_entity_name("hyperbolic-cosine");
  program.give_evaluation({{{real}}, {}});
  program.give_internal(make_unary(
      type_v<long double>, [](long double self) { return cosh(self); }));
  program.give_special_function_variables();
  program.pend_entity();
  program.start_function();
  program.give_entity_name("hyperbolic-tangent");
  program.give_evaluation({{{real}}, {}});
  program.give_internal(make_unary(
      type_v<long double>, [](long double self) { return tanh(self); }));
  program.give_special_function_variables();
  program.pend_entity();
  program.start_function();
  program.give_entity_name("hyperbolic-arc-sine");
  program.give_evaluation({{{real}}, {}});
  program.give_internal(make_unary(
      type_v<long double>, [](long double self) { return asinh(self); }));
  program.give_special_function_variables();
  program.pend_entity();
  program.start_function();
  program.give_entity_name("hyperbolic-arc-cosine");
  program.give_evaluation({{{real}}, {}});
  program.give_internal(make_unary(
      type_v<long double>, [](long double self) { return acosh(self); }));
  program.give_special_function_variables();
  program.pend_entity();
  program.start_function();
  program.give_entity_name("hyperbolic-arc-tangent");
  program.give_evaluation({{{real}}, {}});
  program.give_internal(make_unary(
      type_v<long double>, [](long double self) { return atanh(self); }));
  program.give_special_function_variables();
  program.pend_entity();
  program.start_function();
  program.give_entity_name("error");
  program.give_evaluation({{{real}}, {}});
  program.give_internal(make_unary(type_v<long double>,
                                   [](long double self) { return erf(self); }));
  program.give_special_function_variables();
  program.pend_entity();
  program.start_function();
  program.give_entity_name("complementary-error");
  program.give_evaluation({{{real}}, {}});
  program.give_internal(make_unary(
      type_v<long double>, [](long double self) { return erfc(self); }));
  program.give_special_function_variables();
  program.pend_entity();
  program.start_function();
  program.give_entity_name("gamma");
  program.give_evaluation({{{real}}, {}});
  program.give_internal(make_unary(
      type_v<long double>, [](long double self) { return tgamma(self); }));
  program.give_special_function_variables();
  program.pend_entity();
  program.start_function();
  program.give_entity_name("log-gamma");
  program.give_evaluation({{{real}}, {}});
  program.give_internal(make_unary(
      type_v<long double>, [](long double self) { return lgamma(self); }));
  program.give_special_function_variables();
  program.pend_entity();
  test_fn = program.start_function().lock();
  program.give_entity_name("ceiling");
  program.give_evaluation({{{real}}, {}});
  program.give_internal(make_unary(
      type_v<long double>, [](long double self) { return ceil(self); }));
  test_function(test_fn, type_v<long double>, real, 1.49L, {});
  test_function(test_fn, type_v<long double>, real, 1.50L, {});
  test_function(test_fn, type_v<long double>, real, 1.51L, {});
  test_function(test_fn, type_v<long double>, real, -1.49L, {});
  test_function(test_fn, type_v<long double>, real, -1.50L, {});
  test_function(test_fn, type_v<long double>, real, -1.51L, {});
  program.give_special_function_variables();
  program.pend_entity();
  test_fn = program.start_function().lock();
  program.give_entity_name("floor");
  program.give_evaluation({{{real}}, {}});
  program.give_internal(make_unary(
      type_v<long double>, [](long double self) { return floor(self); }));
  test_function(test_fn, type_v<long double>, real, 1.49L, {});
  test_function(test_fn, type_v<long double>, real, 1.50L, {});
  test_function(test_fn, type_v<long double>, real, 1.51L, {});
  test_function(test_fn, type_v<long double>, real, -1.49L, {});
  test_function(test_fn, type_v<long double>, real, -1.50L, {});
  test_function(test_fn, type_v<long double>, real, -1.51L, {});
  program.give_special_function_variables();
  program.pend_entity();
  test_fn = program.start_function().lock();
  program.give_entity_name("truncate");
  program.give_evaluation({{{real}}, {}});
  program.give_internal(make_unary(
      type_v<long double>, [](long double self) { return trunc(self); }));
  test_function(test_fn, type_v<long double>, real, 1.49L, {});
  test_function(test_fn, type_v<long double>, real, 1.50L, {});
  test_function(test_fn, type_v<long double>, real, 1.51L, {});
  test_function(test_fn, type_v<long double>, real, -1.49L, {});
  test_function(test_fn, type_v<long double>, real, -1.50L, {});
  test_function(test_fn, type_v<long double>, real, -1.51L, {});
  program.give_special_function_variables();
  program.pend_entity();
  test_fn = program.start_function().lock();
  program.give_entity_name("round");
  program.give_evaluation({{{real}}, {}});
  program.give_internal(make_unary(
      type_v<long double>, [](long double self) { return round(self); }));
  test_function(test_fn, type_v<long double>, real, 1.49L, {});
  test_function(test_fn, type_v<long double>, real, 1.50L, {});
  test_function(test_fn, type_v<long double>, real, 1.51L, {});
  test_function(test_fn, type_v<long double>, real, -1.49L, {});
  test_function(test_fn, type_v<long double>, real, -1.50L, {});
  test_function(test_fn, type_v<long double>, real, -1.51L, {});
  program.give_special_function_variables();
  program.pend_entity();
}
} // namespace tonal
