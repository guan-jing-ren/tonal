#include "parameter.hpp"

#include <string>

using namespace std;

namespace tonal {
bool Parameter::is_variadic() const {
  return token && token->find("...") != string::npos;
}

bool Parameter::is_fully_defined() const {
  return evaluation.is_fully_defined();
}
} // namespace tonal
