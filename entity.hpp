#pragma once

#include "parameter.hpp"

#include <variant.hpp>

#include <deque>
#include <memory>

namespace tonal {
struct Entity {
  gut::optional<
      gut::variant<std::weak_ptr<struct Module>, std::weak_ptr<struct Class>,
                   std::weak_ptr<struct Function>>>
      encloser;
  std::weak_ptr<const struct Name> name;
  std::deque<Parameter> parameters;

  bool is_fully_defined() const;
};
} // namespace tonal
