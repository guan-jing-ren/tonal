#include "program.hpp"

#include "class.hpp"
#include "function.hpp"
#include "list.hpp"
#include "module.hpp"
#include "name.hpp"
#include "variable.hpp"

#include <iostream>
#include <numeric>
#include <string>

using namespace std;
using namespace gut;

string to_string(const deque<byte> &b) {
  string s;
  transform(cbegin(b), cend(b), back_inserter(s),
            &type<char>::cast<const byte &>);
  return s;
}

string to_string(const unique_ptr<tonal::List> &b) {
  string s;
  s += '(';
  s += accumulate(
      cbegin(b->cells), cend(b->cells), string{}, [](auto &&s, auto &&cell) {
        cell.value.each([&s](const tonal::Evaluation &eval) {
          eval.value.value().each([&s](auto &&eval) {
            if constexpr (auto vtype = type_v<decltype(eval)>.remove_cvref();
                          vtype.is_arithmetic() ||
                          vtype == type_v<unique_ptr<tonal::List>>) {
              s += to_string(eval);
              if constexpr (vtype == type_v<int64_t>)
                s += 'L';
              else if constexpr (vtype == type_v<long double>)
                s += 'F';
            } else if constexpr (vtype == type_v<deque<byte>>) {
              s += "'";
              s += to_string(eval);
              s += "'";
            }
          });
          s += ' ';
        });
        cell.value.each([&s](const tonal::Token &token) {
          s += '?' + *token + '?';
          s += ' ';
        });
        cell.value.each([&s](const unique_ptr<tonal::List> &list) {
          s += to_string(list);
          s += ' ';
        });
        return move(s);
      });
  if (s.size() - 1)
    s.pop_back();
  s += ')';
  return s;
};

namespace tonal {
static auto start_entity = [](auto &&entity, auto &define_state,
                              const auto &type_string,
                              const auto &nontype_string, auto &import_state,
                              auto predicate) {
  if (!define_state) {
    define_state =
        type_v<decltype(define_state)>.remove_cvref().construct(entity);
    return move(entity);
  }

  define_state.value().each([&entity, &type_string, &nontype_string,
                             predicate](auto &enclosing) {
    constexpr type enclose_type = type_v<decltype(enclosing)>.remove_cvref();
    if constexpr (predicate(enclose_type))
      throw runtime_error{"Attempting to define " + string{type_string} +
                          " inside " + string{nontype_string} + "."};
    else {
      entity->set_encloser(enclosing);
      if constexpr (enclose_type == type_v<shared_ptr<Module>> ||
                    enclose_type == type_v<shared_ptr<Class>>) {
        constexpr type entity_type = type_v<decltype(entity)>.remove_cvref();
        if constexpr (entity_type == type_v<shared_ptr<Module>>)
          enclosing->member_modules.push_back(entity);
        else if constexpr (entity_type == type_v<shared_ptr<Class>>)
          enclosing->member_classes.push_back(entity);
        else if constexpr (entity_type == type_v<shared_ptr<Function>>)
          enclosing->member_functions.push_back(entity);
        else
          enclosing->member_variables.push_back(entity);
      }
    }
  });

  define_state.value() = entity;
  import_state.push_import(entity);
  return move(entity);
};

weak_ptr<Module> Program::start_module() {
  return start_entity(make_shared<Module>(), define_state, "module",
                      "non-module", import_state,
                      [](auto enclose_type) constexpr->decltype(auto) {
                        return enclose_type != type_v<shared_ptr<Module>>;
                      });
}

static auto start_nonmodule = [](auto &&... args) {
  return start_entity(args..., [](auto enclose_type) constexpr->decltype(auto) {
    return enclose_type == type_v<shared_ptr<Variable>>;
  });
};

weak_ptr<Class> Program::start_class() {
  return start_nonmodule(make_shared<Class>(), define_state, "class",
                         "variable", import_state);
}

weak_ptr<Function> Program::start_function() {
  return start_nonmodule(make_shared<Function>(), define_state, "function",
                         "variable", import_state);
}

weak_ptr<Variable> Program::start_variable() {
  return start_nonmodule(make_shared<Variable>(), define_state, "variable",
                         "variable", import_state);
}

shared_ptr<const Name>
Program::make_name(const string_view &name,
                   const shared_ptr<const Name> &parent_name) {
  auto token = token_pool[name];
  auto [name_begin, name_end] = token_to_name.equal_range(token);
  name_begin = find_if(name_begin, name_end, [&parent_name](auto &&token_name) {
    return token_name.second->parent.lock() == parent_name;
  });
  if (name_begin == name_end)
    name_begin = token_to_name.emplace(token, [&token, &parent_name]() {
      auto name = make_shared<Name>();
      name->token = token;
      name->parent = parent_name;
      return name;
    }());
  return name_begin->second;
}

shared_ptr<const Name> Program::make_name(const deque<string_view> &segments) {
  return accumulate(segments.cbegin(), segments.cend(),
                    shared_ptr<const Name>{},
                    [this](auto &&parent, auto &&token) mutable {
                      return make_name(token, parent);
                    });
}

deque<shared_ptr<const Name>>
Program::ends_with(const shared_ptr<const Name> &suffix) const {
  auto [suffix_first, suffix_last] = token_to_name.equal_range(suffix->token);
  deque<shared_ptr<const Name>> matching;
  transform(suffix_first, suffix_last, back_inserter(matching),
            [](auto &&suffix) { return suffix.second; });
  matching.erase(
      remove_if(matching.begin(), matching.end(),
                [this, &suffix](auto &&match) {
                  auto suffix_segment = suffix;
                  for (auto match_segment = match;
                       match_segment && suffix_segment;
                       match_segment = match_segment->parent.lock(),
                            suffix_segment = suffix_segment->parent.lock())
                    if (suffix_segment->token != match_segment->token)
                      return true;
                  return suffix_segment ||
                         overloads.find(match) == overloads.cend();
                }),
      matching.end());
  return matching;
}

void Program::give_entity_name(const string_view &name) {
  if (!define_state)
    throw runtime_error{"No entity defined to give name to."};
  define_state.value().each([this, &name](auto &entity) {
    entity->name =
        make_name(name, entity->encloser
                            ? entity->encloser.value()
                                  .reduce([](auto &&parent) -> decltype(auto) {
                                    return parent.lock()->name;
                                  })
                                  .value()
                                  .lock()
                            : shared_ptr<const Name>{});
    overloads.emplace(entity->name, entity);
  });
}

void Program::add_param_name(const string_view &name) {
  if (!define_state)
    throw runtime_error{"No entity defined to add a parameter to."};
  define_state.value().each([this, &name](auto &entity) {
    entity->parameters.emplace_back(Parameter{token_pool[name], {}});
  });
}

void Program::give_param_evaluation(Evaluation evaluation, gut::count_t param) {
  if (!define_state)
    throw runtime_error{"No entity defined to give a parameter evaluation to."};
  define_state.value().each([&evaluation, param](auto &entity) mutable {
    constexpr type entity_type = type_v<decltype(entity)>.remove_cvref();
    if constexpr (entity_type == type_v<shared_ptr<Function>> ||
                  entity_type == type_v<shared_ptr<Variable>>)
      if (entity->evaluation.type || entity->evaluation.value)
        throw runtime_error{"Attempt to give parameter evaluation after entity "
                            "evaluation has been defined."};

    if (param == -1)
      param = entity->parameters.size() - 1;

    if (entity->parameters.empty())
      throw runtime_error{"Entity has no parameters to give evaluation."};
    if (static_cast<size_t>(param) >= entity->parameters.size())
      throw runtime_error{
          "Attempt to give non-existent parameter an evaluation."};
    if (entity->parameters[param].evaluation.type ||
        entity->parameters[param].evaluation.value)
      throw runtime_error{"Attempt to redefine parameter evaluation."};
    entity->parameters[param].evaluation = move(evaluation);
  });
}

void Program::add_base_class(const weak_ptr<Class> &) {
  if (!define_state)
    throw runtime_error{"No entity defined to add base class to."};
  define_state.value().each([](auto &entity) {
    constexpr type entity_type = type_v<decltype(entity)>.remove_cvref();
    if constexpr (entity_type == type_v<shared_ptr<Class>>) {
      if (!entity->captures.empty() || !entity->member_variables.empty() ||
          !entity->member_functions.empty() || !entity->member_classes.empty())
        throw runtime_error{"Attempt to add base class after starting capture "
                            "or member definitions."};
    } else
      throw runtime_error{
          "Attempt to add a base class to a non-class definition."};
  });
}

void Program::add_capture(const shared_ptr<string> &) { ; }

void Program::give_evaluation(Evaluation evaluation) {
  if (!define_state)
    throw runtime_error{"No entity defined to give an evaluation to."};
  define_state.value().each([&evaluation](auto &entity) {
    constexpr type entity_type = type_v<decltype(entity)>.remove_cvref();
    if constexpr (entity_type == type_v<shared_ptr<Function>> ||
                  entity_type == type_v<shared_ptr<Variable>>) {
      if (entity->evaluation.type || entity->evaluation.value)
        throw runtime_error{"Attempt to redefine entity evaluation."};
      entity->evaluation = move(evaluation);
    } else
      throw runtime_error{"Entity does not have evaluation."};
  });
}

void Program::give_internal(
    function<void(Evaluation &, Evaluation &, const deque<Parameter> &)> fn) {
  define_state.value().each(
      [fn = move(fn)](shared_ptr<Function> &memfn) mutable {
        memfn->internal = move(fn);
      });
}

void Program::pend_entity() {
  if (!define_state)
    throw runtime_error{"No entity defined to pend."};

  auto encloser = define_state.value()
                      .reduce([](auto &entity) { return entity->encloser; })
                      .value();
  if (encloser)
    encloser.value().each([this](auto &encloser) {
      int hit_count = 0;
      while (
          !import_state.import_stack.empty() &&
              import_state.top_entity()
                  .reduce([&encloser](auto &top) {
                    top->encloser.value().reduce([&encloser](
                                                     auto &top_encloser) {
                      constexpr type encloser_type =
                          type_v<decltype(encloser.lock())>.remove_cvref();
                      constexpr type top_type =
                          type_v<decltype(top_encloser.lock())>.remove_cvref();
                      if constexpr (encloser_type == top_type)
                        return top_encloser.lock() != encloser.lock();
                      else
                        return true;
                    });
                  })
                  .value() ||
          ++hit_count == 1)
        import_state.pop_import();
      define_state.value() = encloser.lock();
    });
  else
    define_state.reset();

}

deque<byte> to_bytes(const string_view &s) {
  deque<byte> bytes;
  transform(cbegin(s), cend(s), back_inserter(bytes),
            &type<byte>::cast<const char &>);
  return bytes;
}

void throw_cannot_evaluation_null_list() {
  throw runtime_error{"Cannot evaluation null list."};
}

void throw_cannot_evaluate_empty_list() {
  throw runtime_error{"Cannot evaluation empty list."};
}

void throw_cannot_evaluate_empty_token() {
  throw runtime_error{"Cannot evaluation empty token."};
}

void throw_numerical_token_contains_non_numeric_characters() {
  throw runtime_error{"Numerical token contains non-numeric characters."};
}

void throw_unrecognized_numerical_literal_prefix() {
  throw runtime_error{"Unrecognized numerical literal prefix."};
}

void throw_binary_floating_point_not_supported() {
  throw runtime_error{"Hexatridecimal floating point not supported."};
}

void throw_octal_floating_point_not_supported() {
  throw runtime_error{"Hexatridecimal floating point not supported."};
}

void throw_hexatridecimal_floating_point_not_supported() {
  throw runtime_error{"Hexatridecimal floating point not supported."};
}

void throw_unclosed_string_literal() {
  throw runtime_error{"Unclosed string literal."};
}

unique_ptr<List> Program::eval(const unique_ptr<List> &list) {
  if (!list)
    throw_cannot_evaluation_null_list();
  if (list->cells.empty())
    throw_cannot_evaluate_empty_list();

  auto list_eval = list->replicate();

  for (size_t i = 1; i < list->cells.size(); ++i)
    list->cells[i].value.each([this, &list_eval, i](auto &cell) {
      constexpr type cell_type = type_v<decltype(cell)>.remove_cvref();
      if constexpr (cell_type == type_v<Token>) {
        string_view token = *cell;
        if (token.empty())
          throw_cannot_evaluate_empty_token();
        if (token.find("'") == 0 || token.find('"') == 0 ||
            token.find("u8'") == 0 || token.find("u8\"") == 0 ||
            token.find("u16'") == 0 || token.find("u16\"") == 0 ||
            token.find("u32'") == 0 || token.find("u32\"") == 0) {
          token.remove_prefix(token.find_first_of("'\"") + 1);
          if (token.empty() || (token.back() != '"' && token.back() != '\''))
            throw_unclosed_string_literal();
          token.remove_suffix(1);
          list_eval->cells[i] = {
              Evaluation{{bootstrap.string}, {to_bytes(token)}}};
        } else if (token.find("R'") == 0 || token.find("R\"") == 0 ||
                   token.find("u8R'") == 0 || token.find("u8R\"") == 0 ||
                   token.find("u16R'") == 0 || token.find("u16R\"") == 0 ||
                   token.find("u32R'") == 0 || token.find("u32R\"") == 0) {
          token.remove_prefix(token.find_first_of("'\"") + 1);
          if (token.empty() || (token.back() != '"' && token.back() != '\''))
            throw_unclosed_string_literal();
          token.remove_suffix(1);
          auto quote = token.substr(0, token.find_first_of('('));
          auto endquote = token.substr(token.size() - quote.size());
          if (quote != endquote)
            throw_unclosed_string_literal();
          token.remove_prefix(quote.size() + 1);
          token.remove_suffix(quote.size());
          if (token.empty() || token.back() != ')')
            throw_unclosed_string_literal();
          token.remove_suffix(1);
          list_eval->cells[i] = {
              Evaluation{{bootstrap.string}, {to_bytes(token)}}};
        } else if (token.find_first_of("-+.0123456789") == 0) {
          bool negative = false;
          if (token[0] == '-')
            negative = true;
          if (token[0] == '-' || token[0] == '+')
            token.remove_prefix(1);

          bool floating = false;
          if (token[0] == '.') {
            floating = true;
            token.remove_prefix(1);
          }

          bool integral = false;
          int base = 10;
          if (token[0] == '0') {
            switch (token[1]) {
            default:
              throw_unrecognized_numerical_literal_prefix();
              break;
            case 'b':
              if (floating)
                throw_binary_floating_point_not_supported();
              integral = true;
              base = 2;
              token.remove_prefix(2);
              break;
            case 'o':
              if (floating)
                throw_octal_floating_point_not_supported();
              integral = true;
              base = 8;
              token.remove_prefix(2);
              break;
            case 'd':
              base = 10;
              token.remove_prefix(2);
              break;
            case 'x':
              base = 16;
              break;
            case 'z':
              if (floating)
                throw_hexatridecimal_floating_point_not_supported();
              integral = true;
              base = 36;
              token.remove_prefix(2);
              break;
            }
          }

          char *l_end = nullptr, *d_end = nullptr;
          int64_t l;
          long double d;
          if (floating || !integral)
            d = strtold(token.cbegin(), &d_end);
          if (integral || !floating)
            l = strtoll(token.cbegin(), &l_end, base);

          if (l_end != token.cend() && d_end != token.cend())
            throw_numerical_token_contains_non_numeric_characters();

          if (d_end > l_end)
            list_eval->cells[i] = {
                Evaluation{{bootstrap.real}, {negative ? -d : d}}};
          else
            list_eval->cells[i] = {
                Evaluation{{bootstrap.integer}, {negative ? -l : l}}};
        } else {
          // Try to resolve token as name.
        }
      }
    });

  return list_eval;
}

void Program::complete() {
  //   while (!pending_definition.empty()) {
  //     auto pending = move(*pending_definition.begin());
  //     pending_definition.erase(pending_definition.begin());
  //     pending.second();
  //   }
}

void Program::give_special_entity_variables() {
  start_variable();
  give_entity_name("@date");
  pend_entity();
  start_variable();
  give_entity_name("@time");
  pend_entity();
  start_variable();
  give_entity_name("@file");
  pend_entity();
  start_variable();
  give_entity_name("@line");
  pend_entity();
  start_variable();
  give_entity_name("@position");
  pend_entity();
  start_variable();
  give_entity_name("@nesting");
  pend_entity();
  start_variable();
  give_entity_name("@counter");
  pend_entity();
  start_variable();
  give_entity_name("@archetype");
  pend_entity();
  start_variable();
  give_entity_name("@name");
  pend_entity();
  start_variable();
  give_entity_name("@parameters");
  pend_entity();
  start_variable();
  give_entity_name("@encloser");
  pend_entity();
  start_variable();
  give_entity_name("@overloads");
  pend_entity();
  start_variable();
  give_entity_name("@readers");
  pend_entity();
  start_variable();
  give_entity_name("@writers");
  pend_entity();
}

void Program::give_special_encloser_variables() {
  start_variable();
  give_entity_name("@members");
  pend_entity();
}

void Program::give_special_module_members() {
  give_special_entity_variables();
  give_special_encloser_variables();
}

void Program::give_special_type_variables() {
  give_special_entity_variables();
  start_variable();
  give_entity_name("@type");
  pend_entity();
  start_variable();
  give_entity_name("@object");
  pend_entity();
}

void Program::give_special_function_variables() {
  give_special_type_variables();
  start_variable();
  give_entity_name("@evaltype");
  pend_entity();
}

void Program::give_special_class_members() {
  auto self = define_state.value().value(type_v<shared_ptr<Class>>).value();
  auto name = *self->name.lock()->token;
  give_special_type_variables();
  start_variable();
  give_entity_name("@size");
  pend_entity();
  start_variable();
  give_entity_name("@alignment");
  pend_entity();
  start_variable();
  give_entity_name("@free_bits");
  pend_entity();
  start_variable();
  give_entity_name("@bases");
  pend_entity();
  start_variable();
  give_entity_name("@derivations");
  pend_entity();
  give_special_encloser_variables();
  start_function();
  give_entity_name(name);
  give_evaluation({{{self}}, {}});
  give_special_function_variables();
  pend_entity();
  start_function();
  give_entity_name(name);
  add_param_name("source");
  give_param_evaluation({{{self}}, {}});
  give_evaluation({{{self}}, {}});
  give_special_function_variables();
  pend_entity();
  start_function();
  give_entity_name("~"s + name);
  give_special_function_variables();
  pend_entity();
  start_function();
  give_entity_name("=");
  add_param_name("source");
  give_param_evaluation({{{self}}, {}});
  give_evaluation({{{self}}, {}});
  give_special_function_variables();
  pend_entity();
}
} // namespace tonal
