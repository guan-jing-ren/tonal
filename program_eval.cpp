#include "program_eval.hpp"

#include "class.hpp"
#include "function.hpp"
#include "list.hpp"
#include "module.hpp"
#include "variable.hpp"

#include <functor.hpp>

#include <functional>
#include <iostream>

using namespace std;
using namespace gut;

namespace tonal {
bool Program::ParseQueue::ParseAction::
operator<(const ParseAction &that) const {
  return tie(phase, level, count) > tie(that.phase, that.level, that.count);
}

enum class FilterAction {
  GATHER,
  CHECK_ONE // Should only be used to check non-class membership, because base
            // classes still need to be checked.
};
template <FilterAction Action>
static conditional_t<
    Action == FilterAction::GATHER,
    ntuple<deque<shared_ptr<const Entity>>, deque<shared_ptr<const Entity>>,
           deque<shared_ptr<const Entity>>>,
    bool>
filter_enclosed(const shared_ptr<const Entity> &encloser,
                const deque<shared_ptr<const Entity>> &candidates,
                constant<Action>) {
  deque<shared_ptr<const Entity>> remaining;
  deque<shared_ptr<const Entity>> enclosed;
  deque<shared_ptr<const Entity>> found;
  for (auto &&candidate : candidates) {
    auto entity = candidate;
    for (count_t depth = 0; entity && entity != encloser;
         ++depth, entity =
                      entity->encloser
                          ? entity->encloser.value()
                                .reduce(&type<shared_ptr<const Entity>>::cast<
                                        shared_ptr<const Entity>>)
                                .value()
                          : nullptr)
      if (entity == encloser) {
        if constexpr (Action == FilterAction::GATHER) {
          depth == 0 ? found.push_back(candidate)
                     : enclosed.push_back(
                           candidate); // Functions don't have members, but have
          // special member variables which are
          // enclosed by the function.
          break;
        } else
          return true;
      }
    if constexpr (Action == FilterAction::GATHER)
      if (!entity)
        remaining.push_back(candidate);
  }

  if constexpr (Action == FilterAction::GATHER)
    return ntuple{move(remaining), move(enclosed), move(found)};
  else
    return false;
}

static ntuple<count_t, deque<shared_ptr<const Entity>>> resolve_hierarchical(
    Program::ParseQueue &queue, const shared_ptr<const Class> &encloser,
    const deque<shared_ptr<const Entity>> &candidates, count_t level);

template <typename EntityType>
static ntuple<count_t, deque<shared_ptr<const Entity>>> resolve_members(
    Program::ParseQueue &queue, const shared_ptr<EntityType> &encloser,
    const deque<shared_ptr<const Entity>> &candidates, count_t level) {
  if (candidates.empty())
    return {level, candidates};

  auto remaining = candidates;
  auto resolve_special = [](auto &&queue, auto &&mem, auto &&remaining,
                            auto &&level) {
    auto [notenclosed, subenclosed, found] =
        filter_enclosed(mem, remaining, constant_v<FilterAction::GATHER>);
    remaining = move(notenclosed);
    for (auto &&f : found)
      queue.add_candidate(Program::ParseQueue::ParsePhase::EXPRESSION, level,
                          f);
    for (auto &&s : subenclosed)
      queue.add_candidate(Program::ParseQueue::ParsePhase::EXPRESSION,
                          level + 1, s);
  };

  auto found =
      stable_partition(remaining.begin(), remaining.end(), arg_v == encloser);
  for (auto first = remaining.cbegin(); first != found; ++first)
    queue.add_candidate(Program::ParseQueue::ParsePhase::EXPRESSION, level,
                        *first);
  remaining.erase(remaining.begin(), found);

  for (auto &&memvr : encloser->member_variables) {
    resolve_special(queue, memvr, remaining, level + 1);
    if (remaining.empty())
      break;
  }

  for (auto &&memfn : encloser->member_functions) {
    resolve_special(queue, memfn, remaining, level + 1);
    if (remaining.empty())
      break;
  }

  count_t base_level = level;

  for (auto &&memcl : encloser->member_classes) {
    auto [acclevel, memremain] =
        resolve_hierarchical(queue, memcl, remaining, level + 1);
    base_level = max(acclevel, base_level);
    remaining = move(memremain);
    if (remaining.empty())
      break;
  }

  if constexpr (type_v<EntityType>.remove_cvref() == type_v<Module>)
    for (auto &&memmd : encloser->member_modules) {
      if (!filter_enclosed(memmd, remaining,
                           constant_v<FilterAction::CHECK_ONE>))
        continue;
      auto [acclevel, memremain] =
          resolve_members(queue, memmd, remaining, level + 1);
      base_level = max(acclevel, base_level);
      remaining = move(memremain);
      if (remaining.empty())
        break;
    }

  level = base_level;

  return ntuple{level, remaining};
}

static ntuple<count_t, deque<shared_ptr<const Entity>>> resolve_hierarchical(
    Program::ParseQueue &queue, const shared_ptr<const Class> &encloser,
    const deque<shared_ptr<const Entity>> &candidates, count_t level) {
  if (candidates.empty())
    return {level, candidates};

  auto [base_level, remaining] =
      resolve_members(queue, encloser, candidates, level);
  level = base_level;
  if (remaining.empty())
    return {level, remaining};

  for (auto &&basecl : encloser->base_classes) {
    auto [acclevel, baseremaining] =
        resolve_hierarchical(queue, basecl.lock(), remaining, level + 1);
    base_level = max(acclevel, base_level);
    remaining = move(baseremaining);
    if (remaining.empty())
      break;
  }
  level = base_level;

  return {level, remaining};
}

static deque<shared_ptr<const Entity>> resolve_encloser(
    Program::ParseQueue &queue, const shared_ptr<const Entity> &encloser,
    const deque<shared_ptr<const Entity>> &candidates, count_t level) {
  if (candidates.empty())
    return candidates;

  auto remaining = candidates;
  if (auto enc_class = static_pointer_cast<const Class>(encloser)) {
    auto [base_level, class_remaining] =
        resolve_hierarchical(queue, enc_class, remaining, level);
    level = base_level;
    remaining = move(class_remaining);
  } else if (auto enc_mod = static_pointer_cast<const Module>(encloser);
             enc_mod && filter_enclosed(encloser, remaining,
                                        constant_v<FilterAction::CHECK_ONE>)) {
    auto [base_level, module_remaining] =
        resolve_members(queue, enc_mod, remaining, level);
    level = base_level;
    remaining = move(module_remaining);
  }
  if (remaining.empty())
    return remaining;

  return encloser->encloser
      .reduce([&queue, remaining = move(remaining),
               level](auto &&encloser) mutable {
        return encloser
            .reduce([&queue, remaining = move(remaining),
                     level](auto &&encloser) mutable {
              return resolve_encloser(queue, encloser.lock(), remaining,
                                      level + 1);
            })
            .value();
      })
      .value();
}

enum class GatherType { HIERARCHICAL, ENCLOSER };
template <GatherType Gather>
static auto
gatherer(Program::ParseQueue &queue,
         const Program::ParseQueue::ResolutionEnvironment &environment,
         const deque<shared_ptr<const Entity>> &candidates, count_t level) {
  if constexpr (Gather == GatherType::HIERARCHICAL)
    return environment.encloser.get()
        .reduce([&queue, &candidates, level](auto &&encloser) {
          constexpr type encloser_type =
              type_v<decltype(encloser)>.remove_cvref();
          if constexpr (encloser_type == type_v<shared_ptr<Class>>)
            return resolve_hierarchical(queue, encloser, candidates, level);
          else
            return ntuple<count_t, deque<shared_ptr<const Entity>>>{level,
                                                                    candidates};
        })
        .value();
  else
    return environment.encloser.get()
        .reduce([&queue, &candidates, level](auto &&encloser) {
          return resolve_encloser(queue, encloser, candidates, level);
        })
        .value();
}

static deque<deque<shared_ptr<const Entity>>>
leveller(Program::ParseQueue &queue) {
  deque<deque<shared_ptr<const Entity>>> leveled_overloads;
  while (!queue.prio.empty()) {
    leveled_overloads.push_back({});
    for (auto current_level = queue.prio.top().level;
         current_level == queue.prio.top().level;
         current_level = queue.prio.top().level) {
      leveled_overloads.back().emplace_back(move(queue.prio.top().candidate));
      queue.prio.pop();
      if (queue.prio.empty())
        break;
    }
  }
  return leveled_overloads;
};

static deque<deque<shared_ptr<const Entity>>>
lazy_leveller(Program::ParseQueue &queue,
              Program::ParseQueue::ResolutionEnvironment environment,
              const Evaluation &eval,
              const deque<shared_ptr<const Entity>> &candidates,
              count_t level) {
  if (!eval.type)
    return {};
  return eval.type.value()
      .reduce([&queue, &environment, &candidates, level](auto &&eval) {
        const auto encloser = Program::arche_type{eval.lock()};
        environment.encloser = gut::ref{encloser};
        gatherer<GatherType::HIERARCHICAL>(queue, environment, candidates,
                                           level);
        return leveller(queue);
      })
      .value();
}

static deque<deque<shared_ptr<const Entity>>>
lazy_leveller(Program::ParseQueue &queue,
              Program::ParseQueue::ResolutionEnvironment environment,
              const deque<deque<shared_ptr<const Entity>>> &levelled_overloads,
              const deque<shared_ptr<const Entity>> &candidates,
              count_t level) {
  if (candidates.empty())
    return {};
  auto remaining = candidates;
  for (auto &&levelled_overload : levelled_overloads) {
    if (remaining.empty())
      break;
    auto acclevel = level;
    for (auto &&overload : levelled_overload) {
      auto overloadcl =
          static_pointer_cast<Class>(const_pointer_cast<Entity>(overload));
      if (!overloadcl)
        continue;
      const auto encloser = Program::arche_type{overloadcl};
      environment.encloser = gut::ref{encloser};
      if (remaining.empty())
        break;
      auto [lev, rem] = gatherer<GatherType::HIERARCHICAL>(queue, environment,
                                                           remaining, level);
      acclevel = max(acclevel, lev + 1);
      remaining = move(rem);
    }
    level = acclevel;
  }
  return leveller(queue);
}

static deque<deque<shared_ptr<const Entity>>>
lazy_leveller(Program::ParseQueue &queue,
              Program::ParseQueue::ResolutionEnvironment environment,
              const Program::ParseQueue::ResolutionResult &result,
              const deque<shared_ptr<const Entity>> &candidates,
              count_t level) {
  return result
      .reduce([&queue, &environment, &candidates, level](auto &&result) {
        return lazy_leveller(queue, environment, result, candidates, level);
      })
      .value();
}

static deque<deque<shared_ptr<const Entity>>>
lazy_leveller(Program::ParseQueue &queue,
              Program::ParseQueue::ResolutionEnvironment environment,
              const std::function<
                  gut::optional<deque<Program::ParseQueue::ResolutionResult>>(
                      Program::ParseQueue::EvaluateExpression)> &result,
              const deque<shared_ptr<const Entity>> &candidates,
              count_t level) {
  if (auto eval_result =
          result(Program::ParseQueue::EvaluateExpression::EXPLICIT_EVALUATE);
      eval_result) {
    switch (eval_result.value().size()) {
    default:
      throw runtime_error{
          "Expected entity candidates for overload resolution, got a list."};
      break;
    case 0:
      return {{}};
    case 1:
      return lazy_leveller(queue, environment, eval_result.value()[0],
                           candidates, level);
    }
  }
  return {};
}

static deque<deque<shared_ptr<const Entity>>> immediate_leveller(
    Program::ParseQueue &queue,
    const Program::ParseQueue::ResolutionEnvironment &environment,
    const deque<shared_ptr<const Entity>> &candidates, count_t level) {
  gatherer<GatherType::ENCLOSER>(queue, environment, candidates, level);
  return leveller(queue);
}

auto Program::ParseQueue::resolve_expression(ResolutionEnvironment environment,
                                             EvaluateExpression how)
    -> gut::optional<deque<ResolutionResult>> {
  deque<variant<ResolutionResult,
                std::function<gut::optional<deque<ResolutionResult>>(
                    EvaluateExpression)>>>
      resolved_expression;

  count_t cell_i = -1;
  for (auto &&cell : environment.expression.get()->cells) {
    ++cell_i;
    cell.value.each([&resolved_expression](const Evaluation &evaluation) {
      resolved_expression.push_back({evaluation.replicate()});
    });
    cell.value.each([this, &environment,
                     &resolved_expression](const unique_ptr<List> &expression) {
      resolved_expression.push_back(
          std::function<gut::optional<deque<ResolutionResult>>(
              EvaluateExpression)>{
              [this, environment = ResolutionEnvironment{
                         environment.program, environment.encloser,
                         environment.args, environment.locals,
                         gut::ref{expression}}](EvaluateExpression how) {
                return resolve_expression(environment, how);
              }});
    });
    cell.value.each([this, &environment, &resolved_expression,
                     cell_i](const shared_ptr<const Name> &name) {
      for (auto &&local : environment.locals.get())
        if (local->name.lock() == name)
          add_candidate(ParsePhase::EXPRESSION, 1, local);

      for (auto &&arg : environment.args.get())
        if (arg->name.lock() == name)
          add_candidate(ParsePhase::EXPRESSION, 2, arg);

      deque<shared_ptr<const Name>> candidate_names =
          environment.program.get().ends_with(name);
      deque<shared_ptr<const Entity>> candidates;
      for (auto &&candidate : candidate_names) {
        for (auto [first_candidate, last_candidate] =
                 environment.program.get().overloads.equal_range(candidate);
             first_candidate != last_candidate; ++first_candidate)
          candidates.push_back(
              first_candidate->second
                  .reduce(&type<shared_ptr<const Entity>>::cast<
                          shared_ptr<const Entity>>)
                  .value());
      }

      if (cell_i == 0 && environment.expression.get()->cells.size() > 1) {
        resolved_expression.push_back(
            std::function<gut::optional<deque<ResolutionResult>>(
                EvaluateExpression)>{[this, &resolved_expression, environment,
                                      candidates = move(candidates)](auto) {
              deque<deque<shared_ptr<const Entity>>> leveled_overloads =
                  resolved_expression[1]
                      .reduce(
                          [this, &environment, &candidates](auto &&encloser) {
                            return lazy_leveller(*this, environment, encloser,
                                                 candidates, 3);
                          })
                      .value();
              for (auto &&overload :
                   immediate_leveller(*this, environment, candidates, 3))
                leveled_overloads.emplace_back(move(overload));
              deque<ResolutionResult> result;
              result.emplace_back(move(leveled_overloads));
              return gut::optional{move(result)};
            }});
      } else
        resolved_expression.push_back(
            immediate_leveller(*this, environment, candidates, 3));
    });
  }

  switch (how) {
  case EvaluateExpression::COMMAND:
    break;
  case EvaluateExpression::EXPLICIT_EVALUATE: {
    auto command = move(resolved_expression.front());
    resolved_expression.pop_front();
    command.reduce(
        [arguments = move(resolved_expression)](auto && /* command */) {
          // CommandArgumentEvaluator cae{command};
          // for (auto &&argument : arguments)
          //   argument.each(cae);
        });
  } break;
  case EvaluateExpression::IMPLICIT_LIST:
    break;
  }

  return {};
}

void Program::ParseQueue::run() {
  while (!prio.empty()) {
    prio.top().action();
    prio.pop();
  }
}

auto Program::eval_implicit_number() {}
auto Program::eval_implicit_integer() {}
auto Program::eval_implicit_real() {}
auto Program::eval_implicit_string() {}
auto Program::eval_implicit_list() {}
auto Program::eval_name(const ListCell &cell) {
  give_entity_name(*cell.value.value(type_v<Token>).value());
}
// Allow 'try again later'.
auto Program::eval_param_name(const ListCell &cell) {
  add_param_name(*cell.value.value(type_v<Token>).value());
}
// Allow 'try again later'.
void Program::eval_param_evaluation(const ListCell &, count_t) {
  // Traverse import stack to find symbol.
}
auto Program::eval_expression() {}
auto Program::eval_params(const unique_ptr<List> &params, ParseQueue &pq,
                          count_t level) {
  count_t param_count = 0;
  deque<std::function<void()>> default_funcs;
  for (auto &cell : params->cells)
    cell.value.each([this, &pq, level, param_count = ++param_count,
                     &default_funcs](auto &list) {
      constexpr type vtype = type_v<decltype(list)>.remove_cvref();
      if constexpr (vtype == type_v<unique_ptr<List>>)
        switch (list->cells.size()) {
        default:
          throw runtime_error{"Parameter entry list contains redundant cells."};
        case 0:
          throw runtime_error{"Parameter entry list is empty."};
        case 3:
          default_funcs.emplace_back(
              [this, &list, restore_define_state = define_state.value(),
               restore_import_state = import_state]() {
                define_state = restore_define_state;
                import_state = restore_import_state;
                list->cells[2];
                eval_expression();
              });
        case 2:
          pq.add_action(ParseQueue::ParsePhase::PARAM_EVAL, level,
                        [this, &list,
                         restore_define_state = define_state.value(),
                         restore_import_state = import_state, param_count]() {
                          define_state = restore_define_state;
                          import_state = restore_import_state;
                          eval_param_evaluation(list->cells[1], param_count);
                        });
        case 1:
          pq.add_action(ParseQueue::ParsePhase::PARAM_NAME, level,
                        [this, &list,
                         restore_define_state = define_state.value(),
                         restore_import_state = import_state]() {
                          define_state = restore_define_state;
                          import_state = restore_import_state;
                          eval_param_name(list->cells[0]);
                        });
          break;
        }
    });

  return ntuple{move(default_funcs)};
}
auto Program::eval_entity(const unique_ptr<List> &list, ParseQueue &pq,
                          count_t level) {
  pq.add_action(ParseQueue::ParsePhase::NAME, level,
                [this, &list, restore_define_state = define_state.value(),
                 restore_import_state = import_state]() {
                  define_state = restore_define_state;
                  import_state = restore_import_state;
                  eval_name(list->cells[1]);
                });
  auto [default_funcs] = eval_params(
      list->cells[2].value.value(type_v<unique_ptr<List>>).value(), pq, level);
  return ntuple{move(default_funcs)};
}
auto Program::eval_base_classes(const unique_ptr<List> &, count_t, ParseQueue &,
                                count_t) {}
auto Program::eval_base_class(const ListCell &) {}
auto Program::eval_captures(const unique_ptr<List> &, count_t, ParseQueue &,
                            count_t) {}
auto Program::eval_capture(const ListCell &) {}
// Allow 'try again later'.
void Program::eval_member(const unique_ptr<List> &list, ParseQueue &pq,
                          count_t level) {
  if (string_view token = *list->cells[0].value.value(type_v<Token>).value();
      token == "module")
    eval_member_module(list, pq, level);
  else if (token == "class")
    eval_member_class(list, pq, level);
  else if (token == "function")
    eval_member_function(list, pq, level);
  else if (token == "let")
    eval_member_variable(list, pq, level);
}
void Program::eval_members(const unique_ptr<List> &list, count_t member_start,
                           ParseQueue &pq, count_t level) {
  for (count_t c = member_start, end = list->cells.size(); c < end; ++c)
    eval_member(list->cells[c].value.value(type_v<unique_ptr<List>>).value(),
                pq, level);
  // if archetype, eval_member
  // else, preserve list
}
void Program::eval_member_module(const unique_ptr<List> &list, ParseQueue &pq,
                                 count_t level) {
  auto def = start_module().lock();
  auto [param_default_funcs] = eval_entity(list, pq, level);
  eval_members(list, 3, pq, level + 1);
  pend_entity();
}
void Program::eval_member_class(const unique_ptr<List> &list, ParseQueue &pq,
                                count_t level) {
  auto def = start_class().lock();
  auto [param_default_funcs] = eval_entity(list, pq, level);
  eval_base_classes(list, 3, pq, level);
  eval_captures(list, 4, pq, level);
  eval_members(list, 5, pq, level + 1);
  pend_entity();
}
void Program::eval_member_function(const unique_ptr<List> &list, ParseQueue &pq,
                                   count_t level) {
  auto def = start_function().lock();
  auto [param_default_funcs] = eval_entity(list, pq, level);
  eval_captures(list, 3, pq, level);
  // for expression in expressions
  //   eval_expression() // try
  pend_entity();
}
void Program::eval_member_variable(const unique_ptr<List> &list, ParseQueue &pq,
                                   count_t level) {
  auto def = start_variable().lock();
  auto [param_default_funcs] = eval_entity(list, pq, level);
  pend_entity();
}
auto Program::eval_push_in_expression() {}
auto Program::eval_pull_in_expression() {}
auto Program::eval_push_out_expression() {}
auto Program::eval_pull_out_expression() {}
auto Program::eval_label_expression() {}
auto Program::eval_jump_expression() {}
auto Program::eval_function_expression() {}
} // namespace tonal
