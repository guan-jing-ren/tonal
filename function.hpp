#pragma once

#include "entity.hpp"

#include <functional>

namespace tonal {
struct Function : Entity {
  Evaluation evaluation;
  std::deque<gut::variant<std::shared_ptr<Variable>, std::shared_ptr<Function>,
                          std::shared_ptr<Class>>>
      symbols;
  std::deque<std::weak_ptr<Variable>> captures;

  std::function<void(Evaluation &, Evaluation &, const std::deque<Parameter> &)>
      internal;

  bool is_fully_defined() const;

  void set_encloser(std::weak_ptr<Module> encloser);
  void set_encloser(std::weak_ptr<Class> encloser);
  void set_encloser(std::weak_ptr<Function> encloser);

  void add_symbol(std::shared_ptr<Class> symbol);
  void add_symbol(std::shared_ptr<Function> symbol);
  void add_symbol(std::shared_ptr<Variable> symbol);
};
} // namespace tonal
