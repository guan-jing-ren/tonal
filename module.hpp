#pragma once

#include "entity.hpp"

namespace tonal {
struct Module : Entity {
  std::deque<std::shared_ptr<Module>> member_modules;
  std::deque<std::shared_ptr<struct Class>> member_classes;
  std::deque<std::shared_ptr<struct Function>> member_functions;
  std::deque<std::shared_ptr<struct Variable>> member_variables;

  void set_encloser(std::weak_ptr<Module> encloser) {
    this->encloser = encloser;
  }
  void set_encloser(std::weak_ptr<Class> encloser) = delete;
  void set_encloser(std::weak_ptr<Function> encloser) = delete;

  void add_member(std::shared_ptr<Module> member) {
    member_modules.emplace_back(move(member));
  }
  void add_member(std::shared_ptr<Class> member) {
    member_classes.emplace_back(move(member));
  }
  void add_member(std::shared_ptr<Function> member) {
    member_functions.emplace_back(move(member));
  }
  void add_member(std::shared_ptr<Variable> member) {
    member_variables.emplace_back(move(member));
  }
};
} // namespace tonal
