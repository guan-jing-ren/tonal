
struct EntityTieBreak {
  template <typename LeftValue, typename RightValue, typename ArgValue>
  int8_t operator()(const LeftValue &, const RightValue &,
                    const ArgValue &) const {
    return 0;
  }

  template <typename Value>
  int8_t operator()(const Value &lparam, const Value &rparam,
                    const Value &arg) const {
    return lparam == rparam && arg == lparam ? 0 : 0;
  }

  int8_t operator()(const something &, const something &,
                    const something &) const {
    return 0;
  }

  int8_t operator()(const Evaluation &lparam, const Evaluation &rparam,
                    const Evaluation &arg) const {
    if (auto lex = (*this)(lparam.type, rparam.type, arg.type); lex != 0)
      return lex;
    return (*this)(lparam.value, rparam.value, arg.value);
  }

  int8_t operator()(const ListCell &lparam, const ListCell &rparam,
                    const ListCell &arg) const {
    return (*this)(lparam.value, rparam.value, arg.value());
  }

  int8_t operator()(const unique_ptr<List> &lparam,
                    const unique_ptr<List> &rparam,
                    const unique_ptr<List> &arg) const {
    return (*this)(lparam->cells, rparam->cells, arg->cells);
  }

  template <typename... Members>
  int8_t operator()(const variant<Members...> &lparam,
                    const variant<Members...> &rparam,
                    const variant<Members...> &arg) const {
    return lparam
        .reduce([this, &rparam, &arg](const auto &lparam) {
          return rparam
              .reduce([this, &lparam, &arg](const auto &rparam) {
                return arg
                    .reduce([this, &lparam, &rparam](const auto &arg) {
                      return (*this)(lparam, rparam, arg);
                    })
                    .value();
              })
              .value();
        })
        .value();
  }

  template <typename Pointee>
  int8_t operator()(const shared_ptr<Pointee> &lparam,
                    const shared_ptr<Pointee> &rparam,
                    const shared_ptr<Pointee> &arg) const {
    if (lparam == rparam && lparam == arg)
      return 0;
    return [this, &lparam, &rparam, &arg]() {
      (void)this, (void)lparam, (void)rparam, (void)arg;
      if constexpr (
          type_v<Pointee>.remove_cvref().template is_derived_from<struct Entity>())
        return (*this)(static_cast<const struct Entity &>(*lparam),
                       static_cast<const struct Entity &>(*rparam),
                       static_cast<const struct Entity &>(*arg));
      else
        return 0;
    }();
  }

  template <typename Pointee>
  int8_t operator()(const weak_ptr<Pointee> &lparam,
                    const weak_ptr<Pointee> &rparam,
                    const weak_ptr<Pointee> &arg) const {
    return (*this)(lparam.lock(), rparam.lock(), arg.lock());
  }

  template <typename Element>
  int8_t operator()(const deque<Element> &lparam, const deque<Element> &rparam,
                    const deque<Element> &arg) const {
    for (auto lparam_first = lparam.cbegin(), lparam_last = lparam.cend(),
              rparam_first = rparam.cbegin(), rparam_last = rparam.cend(),
              arg_first = arg.cbegin(), arg_last = arg.cend();
         lparam_first != lparam_last && rparam_first != rparam_last &&
         arg_first != arg_last;
         ++lparam_first, ++rparam_first, ++arg_first)
      if (auto lex = (*this)(*lparam_first, *rparam_first, *arg_first);
          lex != 0)
        return lex;
    return 0;
  }

  int8_t operator()(const struct Parameter &lparam,
                    const struct Parameter &rparam,
                    const struct Parameter &arg) const {
    return (*this)(lparam.evaluation, rparam.evaluation, arg.evaluation);
  }

  int8_t operator()(const struct Entity &lparam, const struct Entity &rparam,
                    const struct Entity &arg) const {
    if (auto lex = (*this)(lparam.encloser, rparam.encloser, arg.encloser);
        lex != 0)
      return lex;
    if (auto lex = (*this)(lparam.name, rparam.name, arg.name); lex != 0)
      return lex;
    return (*this)(lparam.parameters, rparam.parameters, arg.parameters);
  }
};