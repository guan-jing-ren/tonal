#pragma once

#include <memory>
#include <string>
#include <unordered_set>

namespace tonal {
struct TokenPool {
  struct HashPred {
    uint32_t operator()(const std::shared_ptr<const std::string> &value) const;
    bool operator()(const std::shared_ptr<const std::string> &l,
                    const std::shared_ptr<const std::string> &r) const;
  };

  std::unordered_set<std::shared_ptr<const std::string>, HashPred, HashPred>
      tokens;
  std::shared_ptr<std::string> candidate = std::make_shared<std::string>();

  struct Token operator[](const std::string &value);
  struct Token operator[](const char *value);
  struct Token operator[](const std::string_view &value);
};
} // namespace tonal
