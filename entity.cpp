#include "entity.hpp"

namespace tonal {
bool Entity::is_fully_defined() const {
  return !name.expired() && [this]() {
    for (const auto &param : parameters)
      if (!param.is_fully_defined())
        return false;
    return true;
  }();
}
} // namespace tonal
