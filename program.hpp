#pragma once

#include "import_stack.hpp"
#include "token.hpp"
#include "token_pool.hpp"

#include <optional.hpp>

#include <chrono>
#include <functional>
#include <queue>
#include <unordered_map>

namespace tonal {
struct Program {
  using arche_type = gut::variant<
      std::shared_ptr<struct Module>, std::shared_ptr<struct Class>,
      std::shared_ptr<struct Function>, std::shared_ptr<struct Variable>>;

  TokenPool token_pool;
  std::unordered_multimap<Token, std::shared_ptr<const struct Name>,
                          std::hash<std::shared_ptr<const std::string>>>
      token_to_name;
  std::unordered_multimap<std::shared_ptr<const Name>, arche_type> overloads;

  std::unordered_map<std::shared_ptr<const struct Entity>, Token>
      entity_filenames;
  std::unordered_map<std::shared_ptr<const struct Entity>,
                     std::chrono::milliseconds>
      last_updates;

  ImportStack import_state;

  gut::optional<arche_type> define_state;
  std::weak_ptr<Module> start_module();
  std::weak_ptr<Class> start_class();
  std::weak_ptr<Function> start_function();
  std::weak_ptr<Variable> start_variable();

  void give_entity_name(const std::string_view &name);

  void add_param_name(const std::string_view &name);
  void give_param_evaluation(struct Evaluation evaluation, gut::count_t = -1);

  void add_base_class(const std::weak_ptr<Class> &base);
  void add_capture(const std::shared_ptr<std::string> &name);
  void give_evaluation(struct Evaluation evaluation);
  void give_internal(std::function<void(Evaluation &, Evaluation &,
                                        const std::deque<struct Parameter> &)>
                         fn);

  void pend_entity();

  template <typename... L> std::unique_ptr<struct List> operator()(L &&... l);

  std::shared_ptr<const Name>
  make_name(const std::string_view &name,
            const std::shared_ptr<const Name> &parent_name);

  std::shared_ptr<const Name>
  make_name(const std::deque<std::string_view> &segments);

  std::deque<std::shared_ptr<const Name>>
  ends_with(const std::shared_ptr<const Name> &) const;

  std::unique_ptr<struct List> eval(const std::unique_ptr<struct List> &);
  struct ParseQueue {
    enum class ParsePhase {
      NAME,
      PARAM_NAME,
      PARAM_EVAL,
      BASE_CLASS,
      CAPTURE,
      EXPRESSION
    };
    struct ParseAction {
      ParsePhase phase;
      gut::count_t level;
      gut::count_t count;
      std::function<void()> action;
      std::shared_ptr<const Entity> candidate;
      bool operator<(const ParseAction &) const;
    };
    std::priority_queue<ParseAction, std::deque<ParseAction>> prio;

    template <typename Action>
    void add_action(ParsePhase phase, gut::count_t level, Action action) {
      prio.push({phase,
                 level,
                 static_cast<gut::count_t>(prio.size()),
                 std::move(action),
                 {}});
    }

    template <typename Candidate>
    void add_candidate(ParsePhase phase, gut::count_t level,
                       Candidate candidate) {
      prio.push({phase,
                 level,
                 static_cast<gut::count_t>(prio.size()),
                 {},
                 std::move(candidate)});
    }

    enum class EvaluateExpression { COMMAND, EXPLICIT_EVALUATE, IMPLICIT_LIST };
    struct ResolutionEnvironment {
      gut::ref<const Program &> program;
      gut::ref<const arche_type &> encloser;
      gut::ref<const std::deque<std::shared_ptr<const Entity>> &> args;
      gut::ref<const std::deque<std::shared_ptr<const Entity>> &> locals;
      gut::ref<const std::unique_ptr<struct List> &> expression;
    };
    using ResolutionResult =
        gut::variant<Evaluation,
                     std::deque<std::deque<std::shared_ptr<const Entity>>>>;
    gut::optional<std::deque<ResolutionResult>>
        resolve_expression(ResolutionEnvironment,
                           EvaluateExpression); // Try to resolve values first,
                                                // esp. object.func type names.

    void run();
  };
  auto eval_implicit_number();
  auto eval_implicit_integer();
  auto eval_implicit_real();
  auto eval_implicit_string();
  auto eval_implicit_list();
  auto eval_entity(const std::unique_ptr<struct List> &, ParseQueue &,
                   gut::count_t);
  auto eval_name(const struct ListCell &);
  auto eval_params(const std::unique_ptr<struct List> &, ParseQueue &,
                   gut::count_t);
  auto eval_param_name(const struct ListCell &);
  void eval_param_evaluation(const struct ListCell &, gut::count_t);
  auto eval_base_classes(const std::unique_ptr<struct List> &, gut::count_t,
                         ParseQueue &, gut::count_t);
  auto eval_base_class(const struct ListCell &);
  auto eval_captures(const std::unique_ptr<struct List> &, gut::count_t,
                     ParseQueue &, gut::count_t);
  auto eval_capture(const struct ListCell &);
  void eval_members(const std::unique_ptr<struct List> &, gut::count_t,
                    ParseQueue &, gut::count_t);
  void eval_member(const std::unique_ptr<struct List> &, ParseQueue &,
                   gut::count_t);
  void eval_member_module(const std::unique_ptr<struct List> &, ParseQueue &,
                          gut::count_t);
  void eval_member_class(const std::unique_ptr<struct List> &, ParseQueue &,
                         gut::count_t);
  void eval_member_function(const std::unique_ptr<struct List> &, ParseQueue &,
                            gut::count_t);
  void eval_member_variable(const std::unique_ptr<struct List> &, ParseQueue &,
                            gut::count_t);
  auto eval_expression();
  auto eval_using_expression();
  auto eval_push_in_expression();
  auto eval_pull_in_expression();
  auto eval_push_out_expression();
  auto eval_pull_out_expression();
  auto eval_label_expression();
  auto eval_jump_expression();
  auto eval_function_expression();

  void complete();

  void give_special_entity_variables();
  void give_special_encloser_variables();
  void give_special_module_members();
  void give_special_type_variables();
  void give_special_function_variables();
  void give_special_class_members();

  struct Bootstrap {
    std::shared_ptr<Module> reserved;
    std::shared_ptr<Class> integer;
    std::shared_ptr<Class> real;
    std::shared_ptr<Class> list;
    std::shared_ptr<Class> string;

    Bootstrap(Program &);

    void give_archetype_functions(
        Program &, std::function<void(Evaluation &, Evaluation &,
                                      const std::deque<Parameter> &)>);
    void
    give_compare_functions(Program &,
                           std::function<void(Evaluation &, Evaluation &,
                                              const std::deque<Parameter> &)>,
                           std::function<void(Evaluation &, Evaluation &,
                                              const std::deque<Parameter> &)>);

    void give_container_functions(
        Program &,
        std::function<void(Evaluation &, Evaluation &,
                           const std::deque<Parameter> &)>,
        std::function<void(Evaluation &, Evaluation &,
                           const std::deque<Parameter> &)>,
        std::function<void(Evaluation &, Evaluation &,
                           const std::deque<Parameter> &)>,
        std::function<void(Evaluation &, Evaluation &,
                           const std::deque<Parameter> &)>);
    void give_list_functions(Program &);
    void give_string_functions(Program &);

    template <typename Type> void give_number_functions(Program &, Type);
    void give_integer_functions(Program &);
    void give_real_functions(Program &);
  } bootstrap{*this};
};
} // namespace tonal
