#pragma once

#include <memory>
#include <string>

namespace tonal {
struct Token : std::shared_ptr<const std::string> {};
} // namespace tonal
