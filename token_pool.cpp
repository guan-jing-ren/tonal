#include "token_pool.hpp"

#include "token.hpp"

namespace tonal {
uint32_t TokenPool::HashPred::
operator()(const std::shared_ptr<const std::string> &value) const {
  return std::hash<std::string>{}(*value);
}
bool TokenPool::HashPred::
operator()(const std::shared_ptr<const std::string> &l,
           const std::shared_ptr<const std::string> &r) const {
  return *l == *r;
}

Token TokenPool::operator[](const std::string &value) {
  return operator[](std::string_view{value});
}

Token TokenPool::operator[](const char *value) {
  return operator[](std::string_view{value});
}

Token TokenPool::operator[](const std::string_view &value) {
  *candidate = value;
  auto loc = tokens.find(candidate);
  if (loc == tokens.cend())
    loc = tokens.emplace(std::make_shared<std::string>(value)).first;
  return {*loc};
}
} // namespace tonal
