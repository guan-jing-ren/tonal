#include "import_stack.hpp"

namespace tonal {
void ImportStack::push_import(std::shared_ptr<struct Module> import) {
  import_stack.emplace_back(move(import));
}

void ImportStack::push_import(std::shared_ptr<struct Class> import) {
  import_stack.emplace_back(move(import));
}

void ImportStack::push_import(std::shared_ptr<struct Function> import) {
  import_stack.emplace_back(move(import));
}

void ImportStack::push_import(std::shared_ptr<struct Variable> import) {
  import_stack.emplace_back(move(import));
}

void ImportStack::pop_import() {
  if (import_stack.empty())
    return;
  return import_stack.pop_back();
}

gut::variant<std::shared_ptr<struct Module>, std::shared_ptr<struct Class>,
             std::shared_ptr<struct Function>, std::shared_ptr<struct Variable>>
    &ImportStack::top_entity() {
  return import_stack.back();
}

const gut::variant<
    std::shared_ptr<struct Module>, std::shared_ptr<struct Class>,
    std::shared_ptr<struct Function>, std::shared_ptr<struct Variable>> &
ImportStack::top_entity() const {
  return import_stack.back();
}
} // namespace tonal
