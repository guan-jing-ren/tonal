#include "resolve_routine.hpp"

#include <iostream>
#include <numeric>
#include <unordered_set>

using namespace std;
using namespace gut;

namespace tonal {
uint32_t ResolveRoutine::JoinHash::
operator()(const deque<type_index> &join) const {
  double joined =
      accumulate(join.cbegin(), join.cend(), 0.,
                 [i = 1](auto result, auto current) mutable {
                   return result + i * 1.5 * hash<type_index>{}(current);
                 });
  return hash<double>{}(joined);
}

any ResolveRoutine::run(any pool, ProgressPolicy policy) {
  for (type_index type = pool.type(); auto &fn = states[type];
       type = pool.type()) {
    pool = fn(*this, pool);
    if (pool.type() == typeid(Yield))
      break;
    else if (type == pool.type())
      throw runtime_error{"Must yield if returning to the same state."};
    else if (pool.type() == typeid(Split))
      pool = run(move(any_cast<Split &>(pool)));

    if (policy == ProgressPolicy::ROUND_ROBIN)
      break;
  }

  return pool;
}

any ResolveRoutine::run(Split split) {
  while (any_of(split.in.cbegin(), split.in.cend(),
                [](auto &&in) { return in.type() != typeid(Yield); }))
    for (auto &in : split.in) {
      if (in.type() != typeid(Yield))
        in = run(move(in), split.policy);
    }

  deque<type_index> join;
  unordered_set<type_index> reduce;
  for (auto &in : split.in) {
    in = move(any_cast<Yield &>(in).value);
    join.emplace_back(in.type());
    reduce.emplace(in.type());
  }
  auto &join_fn = joins[join];
  if (auto &reduce_fn = reduces[*reduce.cbegin()];
      reduce.size() == 1 && reduce_fn) {
    if (join_fn)
      throw runtime_error{"Ambiguous option between join and reduce routines."};
    return reduce_fn(*this, move(split.in));
  } else
    return join_fn(*this, move(split.in));
}
} // namespace tonal
