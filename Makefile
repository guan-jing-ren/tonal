SHELL=/bin/bash
.SHELLFLAGS=-O extglob -c
CXX=~/clang/bin/clang++
CXXFLAGS=-std=c++17 -Os -g -gsplit-dwarf -Wall -Wextra -Wpedantic -Werror -stdlib=libc++  -I ../gut/ -pthread

%.so: %.cpp %.hpp Makefile
	$(CXX) $(CXXFLAGS) -shared -fPIC $< -o $@

tonal: $(addsuffix .so, $(basename $(wildcard *.cpp))) $(wildcard *.hpp)
	$(CXX) -stdlib=libc++ -pthread !(tonal).so tonal.so -o $@

clean:
	- rm *.so
