#include "list.hpp"

#include <functional>

using namespace gut;
using namespace std;

namespace tonal {
ListCell ListCell::replicate() const {
  return move(value
                  .reduce([](auto &&value) {
                    constexpr type value_type =
                        type_v<decltype(value)>.remove_cvref();
                    if constexpr (value_type == type_v<Evaluation>)
                      return ListCell{value.replicate()};
                    else if constexpr (value_type == type_v<unique_ptr<List>>)
                      return ListCell{value->replicate()};
                    else
                      return ListCell{value};
                  })
                  .value());
}

deque<ListCell>::iterator
List::expand_into(unique_ptr<List> list, deque<ListCell> &expanded,
                  deque<ListCell>::iterator insert_point) {
  for (auto &&cell : list->cells)
    insert_point = ++expanded.emplace(insert_point, move(cell));
  return insert_point;
}

List::~List() {
  deque<ListCell> flattened;
  while (!cells.empty()) {
    for (auto &cell : cells)
      cell.value.each([&flattened](unique_ptr<List> &cell) {
        flattened = move(cell->cells);
      });
    cells.clear();
    cells = move(flattened);
  }
}

unique_ptr<List> List::replicate() const {
  auto list = make_unique<List>();
  transform(cells.cbegin(), cells.cend(), back_inserter(list->cells),
            mem_fn(&ListCell::replicate));
  return list;
}
} // namespace tonal
