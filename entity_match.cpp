#include "entity_match.hpp"

#include "class.hpp"
#include "closest_match.hpp"
#include "evaluation.hpp"
#include "function.hpp"
#include "list.hpp"
#include "name.hpp"
#include "parameter.hpp"
#include "variable.hpp"

#include <unordered_set>

using namespace gut;
using namespace std;

namespace tonal {
void EntityMatch::operator()(const something &, const something &,
                             ClosestMatch &max_match) const {
  max_match.set_unsubstitutable();
}

void EntityMatch::operator()(const Evaluation &param, const Evaluation &arg,
                             ClosestMatch &max_match) const {
  if (!max_match.incompatible() && param.value && arg.value) {
    ClosestMatch value_match;
    (*this)(param.value.value(), arg.value.value(), value_match);
    if (!value_match.equal_value())
      max_match.set_unsubstitutable();

    if (value_match.substitutable() || value_match.param_name_substitutable())
      max_match.set_compatible_type();

    if (value_match.not_substitutable()) {
      max_match.set_unsubstitutable();
      return;
    }
  } else if (!max_match.incompatible() && param.value && !arg.value)
    max_match.set_unsubstitutable();
  else
    max_match.set_unequal_value();

  if (!param.type || !arg.type ||
      param.type.value().index() != arg.type.value().index()) {
    max_match.set_unsubstitutable();
    return;
  }

  if (!max_match.incompatible() && param.type && arg.type)
    if (param.type.value().index() == arg.type.value().index())
      // Try type equality function.
      // Try construction->equality.
      (*this)(param.type.value(), arg.type.value(), max_match);
}

void EntityMatch::operator()(const ListCell &param, const ListCell &arg,
                             ClosestMatch &max_match) const {
  (*this)(param.value, arg.value, max_match);
}

void EntityMatch::operator()(const unique_ptr<List> &param,
                             const unique_ptr<List> &arg,
                             ClosestMatch &max_match) const {
  (*this)(param->cells, arg->cells, max_match);
}

void EntityMatch::operator()(const struct Parameter &param,
                             const struct Parameter &arg,
                             ClosestMatch &max_match) const {
  (*this)(param.evaluation, arg.evaluation, max_match);
}

void EntityMatch::operator()(const struct Class &param, const struct Class &arg,
                             ClosestMatch &max_match) const {
  if (!param.name.expired() && !param.parameters.empty()) {
    (*this)(static_cast<const struct Entity &>(param),
            static_cast<const struct Entity &>(arg), max_match);
    if (max_match.not_substitutable())
      return;
  }

  // Check arg has the members in param. arg can have extra members, but aren't
  // considered.
  // Check member names first. If member name in param is missing in arg,
  // not_substitutable(missing member).

  // Highest match param member set x arg member set.
  // Disambiguate matches param member match set x arg member match
  // If cannot disambiguate at all, not_substitutable(ambiguous param member).
  // Remove from param member set.
  // If more than one arg member matches to the same param member,
  // not_substitutable(ambiguous arg member).

  unordered_set<shared_ptr<Function>> all_matches;

  const ClosestMatch member_equal_type = []() {
    ClosestMatch m;
    m.set_unequal_value();
    return m;
  }();
  // const ClosestMatch member_compatible = []() {
  //   ClosestMatch m;
  //   m.set_incompatible_type();
  //   return m;
  // }();
  for (const auto &pmfn : param.member_functions) {
    ClosestMatch match_result;
    deque<shared_ptr<Function>> matching;
    for (const auto &amfn : arg.member_functions) {
      ClosestMatch candidate_result;
      (*this)(pmfn, amfn, candidate_result);
      if (candidate_result < match_result || matching.empty()) {
        match_result = candidate_result;
        matching = {{amfn}};
      } else if (!(candidate_result > match_result))
        matching.push_back(amfn);
    }
    if (matching.empty()) {
      max_match.set_unsubstitutable(/* No match for pmfn. */);
      return;
    } else if (matching.size() > 1) {
      // Tiebreak.
      max_match.set_unsubstitutable(/* Ambiguous match for pmfn. */);
      return;
    } else if (!all_matches.insert(matching[0]).second) {
      max_match.set_unsubstitutable(/*  */);
      return;
    }

    if (match_result.not_substitutable()) {
      max_match.set_unsubstitutable();
      return;
    }

    if (match_result < member_equal_type)
      max_match.set_incompatible_type();
  }
}

void EntityMatch::operator()(const Entity &param, const Entity &arg,
                             ClosestMatch &max_match) const {
  if (param.name.expired())
    max_match.set_name_unspecified();
  else if (!arg.name.expired())
    (*this)(param.name, arg.name, max_match);
  else
    max_match.set_unsubstitutable();

  if (max_match.not_substitutable())
    return;

  if (param.encloser) {
    if (arg.encloser)
      param.encloser.value().each([&arg, &max_match](auto &&param) {
        arg.encloser.value().each([&param, &max_match](auto &&arg) {
          constexpr auto param_type = type_v<decltype(param)>.remove_cvref();
          constexpr auto arg_type = type_v<decltype(arg)>.remove_cvref();
          if constexpr (param_type == arg_type)
            if (!param.expired() && !arg.expired()) {
              if (param.lock() != arg.lock())
                max_match.set_unsubstitutable();
              else
                max_match.set_unsubstitutable();
            }
        });
      });
    else
      max_match.set_unsubstitutable();
  }

  if (max_match.not_substitutable())
    return;

  // When matching class member entity, handle a_param.is_variadic case.
  auto a_first = arg.parameters.cbegin(), a_last = arg.parameters.cend();
  for (auto &&p_param : param.parameters)
    if (a_first == a_last) {
      if (!p_param.is_variadic())
        max_match.set_unsubstitutable();
      break;
    } else if (p_param.is_variadic())
      for (; a_first != a_last; ++a_first) {
        ClosestMatch list_param_match;
        (*this)(p_param, *a_first, list_param_match);
        if (list_param_match.not_substitutable())
          break;
        if (!list_param_match.equal_value())
          max_match.set_unequal_value();
        if (list_param_match.compatible_type())
          max_match.set_incompatible_type();
        // set max_match based on list_param_match
      }
    else
      (*this)(p_param, *a_first, max_match);
  if (a_first != a_last)
    max_match.set_unsubstitutable();
  // (*this)(param.parameters, arg.parameters, max_match);
}
} // namespace tonal
